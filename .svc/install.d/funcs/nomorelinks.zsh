nomorelinks() {
	dbg ext "[DIR] [INIT] ARGS: '${@}'"

	for l ("${@}") {
		if [[ -n ${DO_NOT_TOUCH} ]] {
			for n ("${(@)DO_NOT_TOUCH}") {
				if [[ "${n}" == "${l}" ]] {
					wrn "[DIR] Skipping '${l}' because it listed in DO_NOT_TOUCH"
					continue
				}
			}
		}

		lf="${HOME}/${l}"
		[[ -e "${lf}" ]] || continue
		if [[ -h "${lf}" ]] {
			dbg ext "[DIR] ${l} is link pointed to: $(linkpath ${lf})"
			if [[ "$(linkpath ${lf})" =~ "${Z}" ]] {
				$noop rm "${lf}"
				dbg inf "[DIR] [OK] ${l} was dropped"
			} else {
				backup "${l}";
				wrn "[DIR] ${l} was moved to backup dir and will not be restored."
				wrn "[DIR] It was a symlink (not into dotfiles repo), while we creating a directory on it's place (so, it will be conflict)"
				wrn "[DIR] Take additional manual action, if needed."
			}
		} elif [[ -d "${lf}" ]] {
			dbg inf "[DIR] [OK] Skipping ${l}, since it is dir."
			continue
		} else {
			backup "${l}";
			wrn "[DIR] ${l} was moved to backup dir and will not be restored."
			wrn "[DIR] It was neither symlink nor directory, while we creating a directory on it's place (so, it will be conflict)"
			wrn "[DIR] Take additional manual action, if needed."
		}
	}
}

