restore() {
	dbg ext "[RESTORE] [INIT] ARGS: '${@}'; CREATE=${+CREATE}"

	for p ("${@}") {
		if [[ -n ${DO_NOT_TOUCH} ]] {
			for n ("${(@)DO_NOT_TOUCH}") {
				if [[ "${n}" == "${p}" ]] {
					wrn "[RESTORE] Skipping '${p}' because it listed in DO_NOT_TOUCH"
					continue
				}
			}
		}
		dbg ext "[RESTORE] Processing: ${p}"
		local b="${H}/.config/backups/${TS}/${p}";
		local f="${H}/${p}" zf="${Z}/${p}";
		local bp="$(linkpath ${b})" zfp="$(linkpath ${zf})";

		if [[ -e "${b}" ]] {
			if [[ -h "${b}" ]] {
				if [[ "${bp}" == "${zfp}" ]] {
					dbg wrn "[RESTORE] [!] '${b}' is linked to '${zfp}'. Skipping it. Should be installed with 'backup --replace'."
					continue
				}
			}
			if [[ -e "${f}" ]] && [[ ! -d "${f}" ]] {
				err "[RESTORE] [!!!] ${p} already exists! Can't restore backup copy before you backing up existing one (normally this shouldn't ever happen)!"
				continue
			}
			${noop} "${(z)MKD}" "$(dirname ${f})/" &&
			if [[ "${p}" =~ '/$' ]] {
				local prt="";
				for c ("${b}"*) {
					local cp=${c##${H}\/}
					if [[ -h "${c}" ]] {
						local clp="$(linkpath ${c})"
						if [[ "${clp}" =~ "${zfp}" ]] {
							dbg wrn "[RESTORE] [!] '${cp}' is a link to '${zfp}'. Skipping it. Should be installed with 'backup --replace'."
							continue
						}
					}
					${noop} "${(z)MV}" "${c}" "${f}${cp}"
					if [[ -e "${c}" ]] {
						prt=1
						err "[RESTORE] [!!!] Failed to restore '${cp}': conflict with existing '${f}${cp}'"
					}
				}
				if [[ -n "${prt}" ]] {
					wrn "[RESTORE] There was errors during restore process. Probably, '${p}' not fully restored. See errors above."
				} else {
					inf "[RESTORE] Content of '${p}' was restored from backup"
				}
			} else {
				${noop} "${(z)MV}" "${b}" "${f}"
				if [[ -e "${b}" ]] {
					err "[RESTORE] [!!!] Failed to restore '${b}': conflict with existing '${f}'"
				} else {
					inf "[RESTORE] '${p}' was restored from backup"
				}
			}
		} else {
			[[ -z "${CREATE}" ]] && dbg wrn "[RESTORE] [!] '${b}' does not exist in the backup. Normally this shouldn't happpen..."
			if [[ -n "${CREATE}" ]] {
				local MKP="";
				if [[ "${p}" =~ '/$' ]] {
					MKP="${MKD}"
				} else {
					MKP="${MKF}"
				}
				if [[ -e "${f}" ]] {
					dbg inf "[CREATE] '${p}' already in place"
					continue
				}
				${noop} "${(z)MKD}" "$(dirname ${f})/" &&
				${noop} "${(z)MKP}" "${f}" &&
				dbg inf "[CREATE] [OK] '${f}' was missed, but created as requested."
			}
		}
	}
}
