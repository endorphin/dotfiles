install() {
	dbg ext "[INSTALL] [INIT] ARGS: '${@}'"

	local allow_minus_files="";

	for p ("${@}") {
		if [[ "${p}" == '^-' ]] {
			if [[ "${p}" == '--' ]] { allow_minus_files=1 } else {
				err "It seems you're misused install function (and passed '${p}' as argument)."
				err "If you trying to use the 'install' command, then call it as 'command install'."
				err "Otherwise, if you really need to copy files starting with 'minus' sign, add '--' as argument."
				continue
			}
		}
		if [[ -n ${DO_NOT_TOUCH} ]] {
			for n ("${(@)DO_NOT_TOUCH}") {
				if [[ "${n}" == "${p}" ]] {
					wrn "[INSTALL] Skipping '${p}' because it listed in DO_NOT_TOUCH"
					continue
				}
			}
		}
		dbg ext "[INSTALL] Processing: ${p}"
		local f="${H}/${p}" zf="${Z}/${p}";
		local b="${H}/.config/backups/${TS}/${p}";
		local fp="$(linkpath ${f})" zfp="$(linkpath ${zf})";

		if [[ -e "${zf}" ]] {
			${noop} "${(z)MKD}" "$(dirname ${f})/" &&
			if [[ "${p}" =~ '/$' ]] {
				dbg err "[INSTALL] == ${p} looks like dir path! we're in 'if =~ /\$ '"
				${noop} "${(z)MKD}" "${f}" &&
				for c ("${zf}"*) {
					local bc="$(basename ${c})"
					local cp="${p}${bc}" cfp="${f}${bc}" czp="$(linkpath ${zf}${bc})"
					backup "${cp}" &&
					${noop} "${(z)LN}" "${czp}" "${cfp}"
				} &&
				inf "[INSTALL] Content of '${p}' was installed"
			} else {
				dbg err "[INSTALL] == ${p} does not looks like dir path! we're in 'else' of 'if =~ /\$ '"
				backup "${p}" &&
				${noop} "${(z)LN}" "${zfp}" "${f}" &&
				inf "[INSTALL] '${p}' was installed"
			}
		} else {
			dbg wrn "[INSTALL] [!] '${p}' is missing in repo. Skipping it."
		}
	}
}

