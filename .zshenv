#!/usr/bin/env zsh
# vim: ft=zsh sw=2 ts=2 et

# Avoid compinit being called two times (Debian #622933)
skip_global_compinit=1

# Avoid corrupting keyboard on sshing in debian ( http://www.zsh.org/mla/users/2014/msg00569.html )
DEBIAN_PREVENT_KEYBOARD_CHANGES=yes

## ^^ fuckin Debian/Ubuntu :( thay makes me insert kludges here :( ^^

test -f "${HOME}/.zshenv_custom" && source "${HOME}/.zshenv_custom"
