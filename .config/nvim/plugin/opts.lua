-- luacheck: globals req vim T
local opt = vim.opt
local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local go = vim.go
-- local f = req'lib.funcs'

local data  = fn.stdpath("data")
local cache = fn.stdpath("cache")

local enabled = T{
  "modeline",
  "modifiable",
  "ttyfast",
  "hidden",
  "showmatch",
  "ignorecase",
  "smartcase",
  "hlsearch",
  "incsearch",
  "showmatch",
  "relativenumber",
  "number",
  -- "ruler",
  -- "title",
  -- "cursorline",
  -- "cursorcolumn",
  "visualbell",
  "showfulltag",
  "writeany",
  "termguicolors",
}

local disabled = T{
  "compatible",
  "showmode",
  -- "showcmd",
  "autoindent",
  "smartindent",
  "cindent",
  -- "wrapscan",
  "wrap",
}

cmd[[filetype on]]
cmd[[filetype plugin on]]
cmd[[filetype indent on]]
cmd[[syntax on]]

local enc = 'utf-8'
opt.encoding = enc
-- opt.termencoding = enc -- removed
opt.fileencoding = enc
opt.fileencodings = {
  "utf-8",
  "cp1251",
  "koi8-r",
  "cp866",
  "latin1"
}

opt.mouse = {}--"a" -- nv
-- opt.scrolljump = 5
-- opt.scrolloff = 3
opt.matchtime = 2

opt.formatoptions = "qn1crjol"
--[=[ ^^^^^^
  q = Format text with gq, but don't format as I type.
  n = gq recognizes numbered lists, and will try to
  1 = break before, not after, a 1 letter word
  c = auto-wrap comments using textwidth, inserting comment leader automatically
  r = Automatically insert the current comment leader after hitting <Enter> in Insert mode
  t = auto-wrap using textwidth
  j = Where it makes sense, remove a comment leader when joining lines.

  see :help fo-table

  default = "tcqj"
--]=]
opt.nrformats = {
  "bin",
  "hex",
  "alpha",
  "octal"
} -- maybe disable alpha? or even hex? (and I also never used octal in real life, IIRC)

opt.viminfo:append",r/tmp" -- don't save backups and swaps for stuff in /tmp

if os.getenv"VIM_NOBACKUP" then
  opt.shada:append(("n/tmp/.%s.shada"):format(os.getenv"USER"))
  disabled:insert"backup"
  disabled:insert"swapfile"
  disabled:insert"undofile"
else
  local backupdir = ("%s/backup"):format(cache)
  if not fn.isdirectory(backupdir) then
    fn.mkdir(backupdir,"p")
  end
  opt.backupdir = backupdir
  opt.directory = ("%s/swap"):format(data)
  -- opt.undodir = ("%s/undo"):format(cache)
  enabled:insert"writebackup"
  enabled:insert"backup"
  enabled:insert"undofile"
  opt.updatetime = 100
  opt.backupext = "~"
  opt.backupskip = {}
end

opt.signcolumn = "number"
opt.numberwidth = 1
opt.matchpairs:append"<:>"
opt.tabstop = 2
opt.shiftwidth = 2
opt.backspace = {
  "indent",
  "eol",
  "start",
}

opt.whichwrap = T{
  "b", "s", -- {back,}space
  "<", ">", -- <left>/<right> in N and V
  "[", "]", -- <left>/<right> in I and R
}:concat()

opt.history = 10000
opt.undolevels = 5000
opt.laststatus = 3

opt.foldmethod = "expr" -- indent, marker, syntax
opt.foldexpr="MyFoldexpr(v:lnum)"
opt.foldtext="MyFoldtext()"

opt.viewoptions = {
  "folds",
  "options",
  "cursor",
  "unix",
  "slash",
}
opt.virtualedit = "onemore"

--[=[
opt.completeopt = {
  "menuone",
  "noselect",
  "noinsert",
}
opt.completeopt:remove("preview")
--]=]

opt.completeopt = {
  "menu",
  "menuone",
  "noselect",
} -- recommended by nvim-cmp readme

-- let @/=''

opt.shortmess="filmnrxoOtTaFWIcqsA"

opt.sessionoptions:append"tabpages"
opt.sessionoptions:append"globals"

-- Just in case it will be needeed whenever:
-- opt.guicursor = nil


-- TODO: check, maybe not needed anymore!
-- hack to stop nvim to detect Konsole when in tmux
-- (I made cursor management stuff in tmux instead).
if os.getenv("TERM"):match"^tmux*" then
  cmd([[let $KONSOLE_DBUS_SERVICE="" | let $KONSOLE_DBUS_SESSION="" | ]]
    ..[[let $KONSOLE_PROFILE_NAME="" | let $KONSOLE_VERSION=""]])
end


-- TODO: rewrite on Lua
-- {{{
cmd[[
function! MyFoldexpr(lnum)
  function! Marker(lnum)
    let l:cur_line = getline(a:lnum)
    let l:next_line = getline(a:lnum + 1)
    let l:marker_start = repeat('{', 3)
    let l:marker_end = repeat('}', 3)
    if l:cur_line =~# l:marker_start
      return "a1"
    elseif l:cur_line =~# l:marker_end
      return "s1"
    else
      return '='
    endif
  endfunction

  let l:ret = Marker(a:lnum)
  return l:ret
endfunction

function! MyFoldtext()
  let l:marker = repeat('{', 3)
  let line = getline(v:foldstart)
  let folded_line_num = v:foldend - v:foldstart
  let line_text = substitute(line, l:marker, '', 'g')
  let fillcharcount = &textwidth - len(line_text) - len(folded_line_num)
  return printf('+%s %d строк: %s%s', repeat('-', 1 + v:foldlevel), folded_line_num, line_text, repeat('·', fillcharcount))
endfunction
]]
-- }}}


if go.term == "linux" then
  opt.background="dark"
  -- colorscheme desert
end

opt.pastetoggle = "<F12>"

--[=[
  opt.t_SI = "\<Esc>]12;purple\x7"
  opt.t_EI = "\<Esc>]12;blue\x7"
--]=]


for _,e in ipairs(enabled) do
  opt[e] = true
end
for _,d in ipairs(disabled) do
  opt[d] = false
end





g.colorcol = 80
g.python_host_prog = nil
g.python3_host_prog = "/usr/bin/python3"

-- I dont't use ruby/perl/js providers
g.loaded_ruby_provider = 0
g.loaded_perl_provider = 0
g.loaded_node_provider = 0


-- TODO: move to filetype-specific config?
g.vimpager_disable_x11 = 1
g.vimpager_passthrough = 1


-- TODO: move to "plugins settings"?
g.localvimrc_name = {
  ".local.vimrc",
  ".vimrc"
}
g.localvimrc_persistent = 2
g.localvimrc_persistence_file = ("%s/localvimrc.persistent"):format(data)

g.vimsyn_embed = "lPr" -- highlight embedded (l)ua, (P)ython and (r)uby syntax
g.vimsyn_folding = "afP" -- support (a)utogroups, (f)unctions, (P)ython-scripts folding
-- g.vimsyn_noerror = 1

g.netrw_http_cmd="curl -sLo"
g.netrw_dav_cmd="curl -sLo"
--g.netrw_menu=0
--g.netrw_use_errorwindow=0
g.netrw_silent=1
g.netrw_bufsettings = 'nohidden noma nomod nonu nowrap ro buflisted'






--[=[
opt.cursorline = true
opt.expandtab = true

opt.tabstop = 2
opt.shiftwidth = 2
opt.softtabstop = 2

-- Copy indent from current line when starting a new line
-- opt.autoindent=true

-- A List is an ordered sequence of items.
opt.list = true
opt.listchars = "tab:-->,trail:·"

-- Minimal number of screen lines to keep above and below the cursor.
opt.scrolloff = 5

-- Time in milliseconds to wait for a key code sequence to complete
opt.timeoutlen = 200
opt.ttimeoutlen = 0
-- no waiting for key combination
opt.timeout = false

-- remember where to recover cursor
opt.viewoptions = 'cursor,folds,slash,unix'

-- [[
-- lines longer than the width of the window will wrap and displaying continues
-- on the next line.
-- ]]
opt.wrap = true
opt.tw = 0
opt.cindent = true
opt.splitright = true
opt.splitbelow = true
opt.showmode = false
opt.showcmd = true

-- auto completion on command
opt.wildmenu = true

-- ignore case when searching and only on searching
opt.ignorecase = true
opt.smartcase = true

vim.cmd("set shortmess+=cwm")
opt.inccommand = 'split'
opt.completeopt = {"menuone", "noselect", "menu"}
opt.ttyfast = true
opt.lazyredraw = true
opt.visualbell = true
opt.updatetime = 100
opt.virtualedit = 'block'
opt.colorcolumn = '100'
opt.lazyredraw = true
opt.signcolumn = "yes:1"
opt.mouse = 'a'

opt.foldmethod = 'indent'
opt.foldlevel = 99
opt.foldenable = true
opt.formatoptions = 'qj'

opt.hidden = true

-- Changed home directory here
local backup_dir = vim.fn.stdpath("cache") .. "/backup"
local backup_stat = pcall(os.execute, "mkdir -p " .. backup_dir)
if backup_stat then
    opt.backupdir = backup_dir
    opt.directory = backup_dir
end

local undo_dir = vim.fn.stdpath("cache") .. "/undo"
local undo_stat = pcall(os.execute, "mkdir -p " .. undo_dir)
local has_persist = vim.fn.has("persistent_undo")
if undo_stat and has_persist == 1 then
    opt.undofile = true
    opt.undodir = undo_dir
end

vim.api.nvim_command('autocmd TermOpen term://* startinsert')
vim.cmd [[set guifont=FiraCode\ Nerd\ Font\ Mono:h15]]

-- neovide specific settings
vim.g.neovide_cursor_vfx_mode = "sonicboom"

-- neoterm specific settings
vim.g.neoterm_default_mod = "botright"
vim.g.neoterm_size = "10"
vim.g.neoterm_automap_keys = "<leader>tt"

-- nvui specific settings
if vim.g.nvui then
    vim.cmd [[NvuiCmdFontFamily FiraCode Nerd Font Mono]]
    vim.cmd [[NvuiCmdFontSize 12]]
    vim.cmd [[NvuiAnimationsEnabled 1]]
    vim.cmd [[NvuiCmdCenterXPos 0.5]]
    vim.cmd [[NvuiCmdCenterYPos 0.2]]
    vim.cmd [[NvuiCmdBorderWidth 3]]
    vim.cmd [[NvuiCmdBorderColor #6E6C6A]]
    vim.cmd [[NvuiCmdBigFontScaleFactor 1.3]]
    vim.cmd [[NvuiCmdPadding 13]]
    vim.cmd [[NvuiPopupMenuBorderWidth 4]]
    vim.cmd [[NvuiPopupMenuBorderColor #6E6C6A]]
    -- nvui g3486971 feature
    vim.cmd [[autocmd InsertEnter * NvuiIMEEnable]]
    vim.cmd [[autocmd InsertLeave * NvuiIMEDisable]]
    -- nvui g87f61c0 feature
    vim.cmd [[hi Normal guisp=#6899B8]]
end
--]=]

-- vim: ts=2 sw=2 et
