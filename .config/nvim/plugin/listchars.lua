-- luacheck: globals vim

local o = vim.o
local nr2char = vim.fn.nr2char
local arr, dot

if o.enc:match"utf" then -- When running in a Unicode environment,
  vim.o.list = true -- visually represent certain invisible characters:

  arr = nr2char(8250) -- using U+203a (    ) for an arrow, and
-- arr = nr2char(9655) -- using U+25B7 (▷) for an arrow, and
  dot = nr2char(8901) -- using U+22C5 (⋅) for a very light dot,
-- dot = nr2char(8226) -- using U+2022 (•) for a very light dot,

  o.listchars=table.concat({
    "tab:"..arr.." ",
    "trail:"..dot,
    "nbsp:" ..dot,
    "precedes:"..nr2char(8592),
    "extends:"..nr2char(8594),
--  "precedes:"..nr2char(8617),
--  "extends:"..nr2char(8618),
  },",")
end
