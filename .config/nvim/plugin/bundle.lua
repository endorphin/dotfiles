-- luacheck: globals req vim

--[=[ -- {{{
map('n','<Space>dw','<cmd>lua require("diaglist").open_all_diagnostics()<CR>',opts)
map('n','<Space>d0','<cmd>lua require("diaglist").open_buffer_diagnostics()<CR>',opts)

require("diaglist").init({
    -- optional settings
    -- below are defaults

    -- increase for noisy servers
    debounce_ms = 50,

    -- list in quickfix only diagnostics from clients
    -- attached to a current buffer
    -- if false, all buffers' clients diagnostics is collected
    buf_clients_only = true, 
})
==]=] -- }}}
