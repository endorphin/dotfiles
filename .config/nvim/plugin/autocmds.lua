-- luacheck: globals vim f req

-- [ Sugar ] {{{
-- local f = req"lib.funcs"

-- local luv = vim.loop
local au  = f.au
local api = vim.api
local cmd = vim.cmd
local fn  = vim.fn

local has    = fn.has
-- local exists = fn.exists
local empty  = fn.empty
local expand = fn.expand
local line   = fn.line

local g = vim.g
local o = vim.o
local b = vim.b

local wo = vim.wo
local bo = vim.bo

local env = vim.env

local function nop()end
-- }}}

-- [[ Relative Numbering ]] {{{
if o.buftype ~= "help" and not(env.MANOPT) and (o.filetype ~= "man" and o.filetype ~= "help") and has("autocmd") then
  -- au BufEnter,FocusGained,InsertLeave * set relativenumber
  -- au BufLeave,FocusLost,InsertEnter   * set norelativenumber
  au("BufEnter,InsertLeave", "*", function() if o.number then o.relativenumber=true end end)
  au("BufLeave,InsertEnter", "*", function() if o.number then o.relativenumber=false end end)
end
-- }}}

-- [ Hooks ] {{{
local function on_AnyBufRead() -- {{{
	local bufnr = api.nvim_get_current_buf()
	local bufname = api.nvim_buf_get_name(bufnr)

	if not(bufname) or #bufname == 0 then
		bufname = expand('.')
	end
	b.filename = bufname

	--[[ local buftype = api.nvim_buf_get_option(bufnr, 'filetype')
	if not(buftype) or #buftype == 0 then
		buftype = fn.join(fn.split(fn.system(("file -s -p -L -z -b --mime-type %s 2>/dev/null"):format(bufname))))
		-- ^^^ takes 15ms startup time on calling time. TODO: Consider rewriting
		if buftype:match("cannot open") then
			buftype = "empty"
		end
	end
	b.filetype = buftype ]]

--	if buftype:match("directory") then
--		g.loaded_netrw = "v999"
--		b.loaded_netrw = "v999"
		--[[
		local tree = req'nvim-tree'
		o.laststatus=0
		if #api.nvim_buf_get_name(bufnr) > 0 then
			cmd"bwipeout"
		end
		req'bufferline.state'.set_offset(31, 'FileTree')
		--tree.open()
		]]
--	end
end -- }}}

local function on_FileType() --{{{
	return
end -- }}}

local function on_AnyBufEnter() -- {{{
--[=[
	function open_url(url)
		vim.g.loaded_netrw=nil
		vim.g.loaded_netrwPlugin=nil
		vim.cmd[[runtime! plugin/netrwPlugin.vim]]
		-- return vim.fn['netrw#BrowseX'](url, vim.fn['netrw#CheckIfRemote']())
		return vim.fn['netrw#FileUrlEdit'](url)
	end
]=]

	local bufnr = api.nvim_get_current_buf()
	local bufname = api.nvim_buf_get_name(bufnr)
	if not(bufname) or #bufname == 0 then
    bufname = "<empty>"
	end
	b.filename = bufname
	local ft = o.filetype
	local ignore_fts = {
		man = true,
	}
	if ignore_fts[ft] then
		return
	else
		if ft == "help" then
			bo.buflisted = true
			cmd[[wincmd o]]
			cmd[[SignifyDisable]]
		end
		if bufname:match('^%w+://%w+') then
			nop()
--			open_url(bufname)
		else
      local wd = expand("%:p:h")
			if fn.isdirectory(wd) > 0 then
        api.nvim_set_current_dir(wd)
      end
		end
	end
	if not(ft == 'NvimTree') then
		o.laststatus = 3
		print('\n') -- hide remains after 'laststatus=0'
	else
		req'bufferline.state'.set_offset(31, 'FileTree')
	end
end -- }}}

local function on_AnyBufLeave() -- {{{
	local ft = o.filetype
	if ft == 'NvimTree' then
		req'bufferline.state'.set_offset(0)
		return
	end
end --}}}

local function on_VimEnter() -- {{{
  if line"$" > 500 then -- XXX: think about value
    o.icm = nil -- disable interactive search and replace preview on big files
  end
end -- }}}

au("FileType", "*", on_FileType)
au("BufRead", "*", on_AnyBufRead)
au("BufEnter", "*", on_AnyBufEnter)
au("BufLeave", "*", on_AnyBufLeave)
au("VimEnter", "*", on_VimEnter)
-- }}}

-- Try to jump to the last spot the cursor was at in a file when reading it. {{{
au("BufReadPost", "*", function()
  if not(g.leave_my_cursor_position_alone) then
    local c = line[['"]]
    if c > 0 and c <= line"$" then
      cmd[[exe "normal! g'\""]]
    end
  end
end)
-- }}}

-- [[ FileType-related ]] {{{
	-- [ Gentoo related ] {{{
		au("BufEnter,BufNewFile,BufRead", "*-*/*/metadata.xml", function() bo.sw=2; bo.ts=2; bo.sts=2; bo.et=true; end)
  -- }}}
	-- [ C/C++ ] {{{
    -- Use :make to compile C/C++, even without a makefile
    -- au FileType c if glob('[Mm]akefile') == "" | let &mp="clang -o %< %" | endif
    -- au FileType cpp if glob('[Mm]akefile') == "" | let &mp="clang++ -o %< %" | endif
  -- }}}
	-- [ other ] {{{
		au("BufWritePost", "~/.Xdefaults", function() cmd[[redraw|echo system('xrdb '.expand('<amatch>'))]] end)
  -- }}}
	-- [ Y(A)ML ] {{{
		au("BufEnter,BufNewFile,BufRead", {"*.sls", "*.yml", "*.yaml"},
			function() bo.ft="yaml"; bo.sw=2; bo.ts=2; bo.sts=2; bo.et=true; end
		)
		au("BufEnter,BufNewFile,BufRead", {"*/playbooks/*.yml", "*/playbooks/*.yaml"},
			function() bo.ft="yaml.ansible"; bo.sw=2; bo.ts=2; bo.sts=2; bo.et = true; end
		)
  -- }}}
	-- [ *Makefile* ] {{{
		au("BufEnter", "?akefile*", function() wo.et = false; wo.cindent = false; end)
  -- }}}
	-- [ JS ] {{{
		au("BufRead,BufNewFile", "*.js", function() wo.ft="javascript.jquery" end)
	-- }}}
	-- [ CSS ] {{{
		au("BufRead,BufNewFile", "*.css", function() wo.ft="css"; wo.syntax="css" end)
	-- }}}
	-- [ PHP ] {{{
		au("BufRead,BufNewFile", "*.php", function()
      wo.foldmethod="manual";
      wo.fen = true
      b.php_sql_query=1	 -- подсветка SQL запросов
      b.php_htmlInStrings=1 -- подсветка HTML кода в PHP строках
      b.php_folding=1	   -- фолдинг функций и классов
    end)
	-- }}}
	-- [ DOC ] {{{
		au("BufReadPre", "*.doc", function() wo.ro=true end)
		au("BufReadPre", "*.doc", function() wo.hlsearch = not(o.hlsearch) end)
		au("BufReadPost", "*.doc", function() cmd[[%!antiword "%"]] end)
	-- }}}
	-- [ CSV ] {{{
    au("BufRead", "*.csv", function() cmd[[setfiletype csv]] end)
    au("BufRead,BufWritePost", "*.csv", function() if line'$' < 100 then cmd[[%ArrangeColumn]] end end)
    au("BufWritePre", "*.csv", function() if line'$' < 100 then cmd[[%UnArrangeColumn]] end end)
  -- }}}
	-- [ nginx (in portage build dir) ] {{{
    au("BufRead", "*nginx*work*/config", function() wo.ft="nginx"; wo.sw=4; wo.ts=4 end)
  -- }}}
	-- [ vim colorizer ] {{{
    -- au BufRead */colors/*.vim let g:colorizer_swap_fgbg=0 " was FileType
  -- }}}
-- }}}

-- [ Pre-Save mkdir (if not exist) ] {{{
local function MkNonExDir(file, buf)
  if empty(fn.getbufvar(buf, '&buftype')) > 0 and not(file:match'^%w+:/') then
    local dir=fn.fnamemodify(file, ':h')
    if fn.isdirectory(dir) == 0 then
      fn.mkdir(dir, 'p')
      api.nvim_set_current_dir(dir)
    end
  end
end

au("BufWritePre", "*", function() MkNonExDir(expand('<afile>'), expand('<abuf>')) end)
-- }}}

-- [[
-- Unsorted ]] {{{

-- In plain-text files and svn commit buffers, wrap automatically at 78 chars
-- au("FileType", "text,svn", function() o.tw=78; o.fo=o.fo.."t"; end)

-- Настройка фолдинга для .vimrc и прочих vim файлов
-- au BufRead,BufNewFile,FileType,Syntax vim setl fdm=marker

-- au InsertLeave * call cursor([getpos('.')[1], getpos('.')[2]+1])


-- au BufNewFile,BufRead *.css,*.html,*.htm,*/colors/*.vim,*.php
-- if line("$") < 1000 | if exists(":ColorHighlight") ==# 2 | ColorHighlight! | endif | endif
-- au CursorMoved,CursorMovedI *
-- if exists("b:colorizer_au_loaded") | exec line("w0").",".line("w$")."ColorHighlight!" | endif
	--function s:ColorizeVisibleLines(start,end)
	--	if exists("b:colorizer_au_loaded") | exec line("w0").",".line("w$")."ColorHighlight!" | endif
	--endfunction
-- }}}

au("FileWritePost", "*", function() b.filetype = nil end)
au("VimLeave", "*",
  function()
    o.guicursor="a:ver1-Cursor/lCursor-blinkon1"
  end
)

