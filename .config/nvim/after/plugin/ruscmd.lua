-- luacheck: globals req vim T, no max line length

-- TODO: think about unhardcoding repeated Mapping Functions declaration (that doubles ones in keys.lua)

-- {{{ Mapping Functions

local function _map(mode, lhs, rhs, opts)
  local f = req'lib.funcs'
  local o = opts or {}
  f.map(mode,lhs,rhs,o)
end

local function map(lhs,rhs,opts)  _map('',lhs,rhs,opts) end
local function nmap(lhs,rhs,opts) _map('n',lhs,rhs,opts) end
local function imap(lhs,rhs,opts) _map('i',lhs,rhs,opts) end
local function vmap(lhs,rhs,opts) _map('v',lhs,rhs,opts) end
local function smap(lhs,rhs,opts) _map('s',lhs,rhs,opts) end -- luacheck: ignore
local function xmap(lhs,rhs,opts) _map('x',lhs,rhs,opts) end -- luacheck: ignore
local function omap(lhs,rhs,opts) _map('o',lhs,rhs,opts) end -- luacheck: ignore
local function lmap(lhs,rhs,opts) _map('l',lhs,rhs,opts) end -- luacheck: ignore
local function cmap(lhs,rhs,opts) _map('c',lhs,rhs,opts) end -- luacheck: ignore
local function tmap(lhs,rhs,opts) _map('t',lhs,rhs,opts) end -- luacheck: ignore
local function map_(lhs,rhs,opts) _map('!',lhs,rhs,opts) end -- luacheck: ignore -- map!

local function noremap(lhs,rhs,opts) local o = opts or {} o.noremap = true map(lhs,rhs,o) end
local function nnoremap(lhs,rhs,opts) local o = opts or {} o.noremap = true nmap(lhs,rhs,o) end
local function inoremap(lhs,rhs,opts) local o = opts or {} o.noremap = true imap(lhs,rhs,o) end -- luacheck: ignore
local function vnoremap(lhs,rhs,opts) local o = opts or {} o.noremap = true vmap(lhs,rhs,o) end -- luacheck: ignore
local function xnoremap(lhs,rhs,opts) local o = opts or {} o.noremap = true xmap(lhs,rhs,o) end -- luacheck: ignore
local function tnoremap(lhs,rhs,opts) local o = opts or {} o.noremap = true tmap(lhs,rhs,o) end -- luacheck: ignore
-- TODO: others when needed

-- }}}

map('й', '<cmd>Sayonara<CR>', { silent = true, unique = true })
-- map('й', '<cmd>q<CR>', { silent = true, unique = true })
map('Й', '<cmd>q!<CR>', { silent = true, unique = true })
map('ц', '<cmd>w<CR>', { silent = true, unique = true })
map('Ц', '<cmd>w!<CR>', { silent = true, unique = true })
map('<m-й>', '<cmd>qa!<CR>', { silent = true, unique = true })

