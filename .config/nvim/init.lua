-- luacheck: globals req vim T

require"lib.globals"

-- Do not source the default filetype.vim as NVim had migrated to filetype.lua,
-- and also we'll load filetype.nvim later (and it's much more faster)
-- and also we'll load polyglot then, which is slow as fuck anyway.
vim.g.do_filetype_lua = 1
vim.g.did_load_filetypes = 0

vim.opt.termguicolors = true -- kludge for packer_compiled

table = req"lib.table" or table -- luacheck: ignore
T = table.create or function(t) return t end -- luacheck: ignore

local packer = req'lib.packer'

if packer then packer:load() end

-- Re-cache all the documentation from all the plugins
-- XXX: (slows down the startup. Maybe somehow rewrite with async/subprocesses?)
-- vim.cmd'helptags ALL'

-- vim.cmd'start' -- auto-enter Insert mode (actually, mostly useless)
