local s = req"specs"

s.setup{
    show_jumps  = false,
    min_jump = 0,
    popup = {
        delay_ms = 200, -- delay before popup displays
        inc_ms = 10, -- time increments used for fade/resize effects
        blend = 10, -- starting blend, between 0-100 (fully transparent), see :h winblend
        width = 100,
        winhl = "PMenu",
        -- fader = s.linear_fader,
        fader = s.pulse_fader,
        -- fader = s.exp_fader,
        resizer = s.shrink_resizer,
        -- resizer = s.slide_resizer,
    },
    ignore_filetypes = {},
    ignore_buftypes = {
        nofile = true,
    },
}

s.hl_toggle = function()
  vim.wo.cursorcolumn = not(vim.wo.cursorcolumn)
  vim.wo.cursorline = not(vim.wo.cursorline)
end

s.hl_blink = function()
  s.hl_toggle()
end

return s
