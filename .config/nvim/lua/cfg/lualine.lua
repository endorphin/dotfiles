-- luacheck: globals req vim T, no max line length
--[=[
 Configuration is not yet finished, my statusline plugin is still much more useful for me
 (although it makes nvim to take 200 additional ms to first render)
--]=]

--local custom_pwl = req"lualine.themes.mva-powerline"
local custom_pwl = req"lualine.themes.powerline_dark"

custom_pwl.normal.c.bg = 'default'

local hi = vim.api.nvim_set_hl
--hi(0, "Modified", {ctermfg=9, ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#ff0000",})
hi(0, "Modified", { fg = '#ff0000', bg = 'NONE', bold=1,})

req'lualine'.setup { -- {{{
  options = { -- {{{
    icons_enabled = true,
    theme = custom_pwl,
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},     -- Filetypes to disable lualine for.
    always_divide_middle = true, -- When set to true, left sections i.e. 'a','b' and 'c'
                                 -- can't take over the entire statusline even
                                 -- if neither of 'x', 'y' or 'z' are present.
    globalstatus = true,         -- enable global statusline (have a single statusline
                                 -- at bottom of neovim instead of one for  every window).
                                 -- This feature is only available in neovim 0.7 and higher.
  }, -- }}}
  sections = { -- {{{
    lualine_a = { -- {{{
      { -- {{{
        'mode',
        icons_enabled = true, -- Enables the display of icons alongside the component.
        -- Defines the icon to be displayed in front of the component.
        -- Can be string|table
        -- As table it must contain the icon as first entry and can use
        -- color option to custom color the icon. Example:
        -- {'branch', icon = ''} / {'branch', icon = {'', color={fg='green'}}}
  --      icon = nil,

  --      separator = nil,      -- Determines what separator to use for the component.
                              -- Note:
                              --  When a string is provided it's treated as component_separator.
                              --  When a table is provided it's treated as section_separator.
                              --  Passing an empty string disables the separator.
                              --
                              -- These options can be used to set colored separators
                              -- around a component.
                              --
                              -- The options need to be set as such:
                              --   separator = { left = '', right = ''}
                              --
                              -- Where left will be placed on left side of component,
                              -- and right will be placed on its right.
                              --

  --      cond = nil,           -- Condition function, the component is loaded when the function returns `true`.

        -- Defines a custom color for the component:
        --
        -- 'highlight_group_name' | { fg = '#rrggbb'|cterm_value(0-255)|'color_name(red)', bg= '#rrggbb', gui='style' } | function
        -- Note:
        --  '|' is synonymous with 'or', meaning a different acceptable format for that placeholder.
        -- color function has to return one of other color types ('highlight_group_name' | { fg = '#rrggbb'|cterm_value(0-255)|'color_name(red)', bg= '#rrggbb', gui='style' })
        -- color functions can be used to have different colors based on state as shown below.
        --
        -- Examples:
        --   color = { fg = '#ffaa88', bg = 'grey', gui='italic,bold' },
        --   color = { fg = 204 }   -- When fg/bg are omitted, they default to the your theme's fg/bg.
        --   color = 'WarningMsg'   -- Highlight groups can also be used.
        --   color = function(section)
        --      return { fg = vim.bo.modified and '#aa3355' or '#33aa88' }
        --   end,
  --      color = nil, -- The default is your theme's color for that section and mode.

        -- Specify what type a component is, if omitted, lualine will guess it for you.
        --
        -- Available types are:
        --   [format: type_name(example)], mod(branch/filename),
        --   stl(%f/%m), var(g:coc_status/bo:modifiable),
        --   lua_expr(lua expressions), vim_fun(viml function name)
        --
        -- Note:
        -- lua_expr is short for lua-expression and vim_fun is short for vim-function.
  --      type = nil,

  --      padding = 1, -- Adds padding to the left and right of components.
                     -- Padding can be specified to left or right independently, e.g.:
                     --   padding = { left = left_padding, right = right_padding }

  --      fmt = nil,   -- Format function, formats the component's output.
      }, -- }}}
    }, -- }}}
    lualine_b = { -- {{{
      'branch',
      { -- {{{
        'diff',
        colored = true, -- Displays a colored diff status if set to true
        diff_color = {
          -- Same color values as the general color option can be used here.
          added    = 'DiffAdd',    -- Changes the diff's added color
          modified = 'DiffChange', -- Changes the diff's modified color
          removed  = 'DiffDelete', -- Changes the diff's removed color you
        },
        symbols = {added = '+', modified = '~', removed = '-'}, -- Changes the symbols used by the diff.
        source = nil, -- A function that works as a data source for diff.
                      -- It must return a table as such:
                      --   { added = add_count, modified = modified_count, removed = removed_count }
                      -- or nil on failure. count <= 0 won't be displayed.
      }, -- }}}
      { -- {{{
        'diagnostics',
        sources={ 'nvim_diagnostic', 'nvim_lsp', }, --, 'coc' }
        -- Displays diagnostics for the defined severity types
        sections = { 'error', 'warn', 'info', 'hint', },
        diagnostics_color = {
          -- Same values as the general color option can be used here.
          error = 'DiagnosticError', -- Changes diagnostics' error color.
          warn  = 'DiagnosticWarn',  -- Changes diagnostics' warn color.
          info  = 'DiagnosticInfo',  -- Changes diagnostics' info color.
          hint  = 'DiagnosticHint',  -- Changes diagnostics' hint color.
        },
        symbols = {
          error = '',
          warn = '',
          info = '',
          hint = ''
        },
        colored = true,           -- Displays diagnostics status in color if set to true.
        update_in_insert = false, -- Update diagnostics in insert mode.
        always_visible = false,   -- Show diagnostics even if there are none.
      } -- }}}
    }, -- }}}
    lualine_c = { -- {{{
      {
        'filename',
        file_status = true,      -- Displays file status (readonly status, modified status)
        path = 0,                -- 0: Just the filename
                                 -- 1: Relative path
                                 -- 2: Absolute path

        shorting_target = 40,    -- Shortens path to leave 40 spaces in the window
                                 -- for other components. (terrible name, any suggestions?)
        symbols = {
          modified = ' %#Statusline#✗',      -- Text to show when the file is modified.
          unmodified = ' ✓',
          readonly = ' ',      -- Text to show when the file is non-modifiable or readonly.
          unnamed = '<empty>',  -- Text to show for unnamed buffers.
        }
      }
    }, -- }}}
    lualine_x = { --{{{
      'encoding',
      {
        'fileformat',
        symbols = {
          unix = '', -- e712
          dos = '',  -- e70f
          mac = '',  -- e711
        }
      },
      {
        'filetype',
        colored = true,   -- Displays filetype icon in color if set to true
        icon_only = false -- Display only an icon for filetype
      },
    }, -- }}}
    lualine_y = {'progress'},
    lualine_z = {'location'}
  }, -- }}}
  inactive_sections = { -- {{{
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  }, -- }}}
  tabline = {
    -- lualine_a = {'buffers'},
    -- lualine_z = {'tabs'},
  },
  extensions = {
    'fzf',
    'nvim-tree',
    'quickfix',
    'symbols-outline',
  },
} -- }}}
