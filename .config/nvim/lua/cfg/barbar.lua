-- luacheck: globals req vim T

-- [ Colors ] {{{

local hi = vim.api.nvim_set_hl

--[[
" " Meaning of terms:
"
" format: "Buffer" + status + part
"
" status:
"     *Current: current buffer
"     *Visible: visible but not current buffer
"    *Inactive: invisible but not current buffer
"
" part:
"        *Icon: filetype icon
"       *Index: buffer index
"         *Mod: when modified
"        *Sign: the separator between buffers
"      *Target: letter in buffer-picking mode
"
" BufferTabpages: tabpage indicator
" BufferTabpageFill: filler after the buffer section
" BufferOffset: offset section, created with set_offset()
--]]

hi(0, "TabLineFill", {bg = "#1B1D1E", fg = "#808080",})
hi(0, "TabLineSel", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "TabLine", {bg = "#1B1D1E", fg = "#808080", italic=1, })
hi(0, "BufferCurrent", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "BufferCurrentIndex", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "BufferCurrentMod", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "BufferCurrentSign", {bg = "#1B1D1E", fg = "#559EFF", bold=1, })
hi(0, "BufferCurrentTarget", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })

hi(0, "BufferVisible", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })
hi(0, "BufferVisibleIndex", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })
hi(0, "BufferVisibleMod", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })
hi(0, "BufferVisibleSign", {bg = "#1B1D1E", fg = "#DD77AA", })
-- gui=bold,italic
hi(0, "BufferVisibleTarget", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })

hi(0, "BufferInactive", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveIndex", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveMod", {bg = "#1B1D1E", fg = "#808080", })
--hi(0, "BufferInactiveSign", {bg=#1B1D1E fg=#808080 gui=none })
hi(0, "BufferInactiveTarget", {bg = "#1B1D1E", fg = "#808080", })

-- }}}
vim.g.bufferline = { -- {{{
  -- Enable/disable animations
  animation = true,

  -- Enable/disable auto-hiding the tab bar when there is a single buffer
  auto_hide = false,

  -- Enable/disable current/total tabpages indicator (top right corner)
  tabpages = true,

  -- Enable/disable close button
  closable = false, --true,

  -- Enables/disable clickable tabs
  --  - left-click: go to buffer
  --  - middle-click: delete buffer
  clickable = false, -- true,

  -- Excludes buffers from the tabline
--  exclude_ft = {'javascript'},
--  exclude_name = {'package.json'},

  -- Enable/disable icons
  -- if set to 'numbers', will show buffer index in the tabline
  -- if set to 'both', will show buffer index and icons in the tabline
  icons = true,

  -- If set, the icon color will follow its corresponding buffer
  -- highlight group. By default, the Buffer*Icon group is linked to the
  -- Buffer* group (see Highlighting below). Otherwise, it will take its
  -- default value as defined by devicons.
  icon_custom_colors = false,

  -- Configure icons on the bufferline.
--  icon_separator_active = '▎',
--  icon_separator_inactive = '▎',
  icon_separator_active = '',
  icon_separator_inactive = '',
  icon_close_tab = '',
  icon_close_tab_modified = '●',
  icon_pinned = '車',

  -- If true, new buffers will be inserted at the start/end of the list.
  -- Default is to insert after current buffer.
  insert_at_end = false,
  insert_at_start = false,

  -- Sets the maximum padding width with which to surround each tab
  maximum_padding = 1,

  -- Sets the maximum buffer name length.
  maximum_length = 30,

  -- If set, the letters for each buffer in buffer-pick mode will be
  -- assigned based on their name. Otherwise or in case all letters are
  -- already assigned, the behavior is to assign letters in order of
  -- usability (see order below)
  semantic_letters = true,

  -- New buffer letters are assigned in this order. This order is
  -- optimal for the qwerty keyboard layout but might need adjustement
  -- for other layouts.
  letters = 'asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP',

  -- Sets the name of unnamed buffers. By default format is "[Buffer X]"
  -- where X is the buffer number. But only a static string is accepted here.
  no_name_title = '',
  } -- }}}
