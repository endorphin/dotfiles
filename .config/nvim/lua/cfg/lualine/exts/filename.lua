-- Based on lualine's bundled filename component, Copyright (c) 2020-2021 shadmansaleh
-- MIT license, see LICENSE for more details.

-- Modifications: Copyright (c) 2022 Vadim "mva" Misbakh-Soloviov
-- parent commit id: 181b14348f513e6f9eb3bdd2252e13630094fdd3
-- TODO: check changes (lua/lualine/components/filename.lua)

-- luacheck: globals vim req

local hi = vim.api.nvim_set_hl

local M = req('lualine.component'):extend()

local modules = req('lualine_require').lazy_require {
  utils = 'lualine.utils.utils',
}

local default_options = {
  symbols = {
          modified = ' ✗',      -- Text to show when the file is modified.
          saved = ' ✓',
          readonly = ' ',      -- Text to show when the file is non-modifiable or readonly.
          unnamed = '<empty>',  -- Text to show for unnamed buffers.
  },
  file_status = true,
  path = 0,
  shorting_target = 40,
}

-- [ Colors ] {{{
hi(0, "Modified", {ctermfg=9,   ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#ff0000",})
hi(0, "Saved", {ctermfg=22,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#00ff00",})
hi(0, "isRO", {ctermfg=196, ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#ff0000",})
hi(0, "FileName", {ctermfg=244, ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#808080",})
-- }}}

---counts how many times pattern occur in base ( used for counting path-sep )
---@param base string
---@param pattern string
---@return number
local function count(base, pattern)
  return select(2, string.gsub(base, pattern, ''))
end

---shortens path by turning apple/orange -> a/orange
---@param path string
---@param sep string path separator
---@return string
local function shorten_path(path, sep)
  -- ('([^/])[^/]+%/', '%1/', 1)
  return path:gsub(string.format('([^%s])[^%s]+%%%s', sep, sep, sep), '%1' .. sep, 1)
end

M.init = function(self, options)
  M.super.init(self, options)
  self.options = vim.tbl_deep_extend('keep', self.options or {}, default_options)
end

M.update_status = function(self)
  local data
  if self.options.path == 1 then
    -- relative path
    data = vim.fn.expand('%:~:.')
  elseif self.options.path == 2 then
    -- absolute path
    data = vim.fn.expand('%:p')
  else
    -- just filename
    data = vim.fn.expand('%:t')
  end

  data = modules.utils.stl_escape(data)

  if data == '' then
    data = self.options.symbols.unnamed
  end

  if self.options.shorting_target ~= 0 then
    local windwidth = self.options.globalstatus and vim.go.columns or vim.fn.winwidth(0)
    local estimated_space_available = windwidth - self.options.shorting_target

    local path_separator = package.config:sub(1, 1)
    for _ = 0, count(data, path_separator) do
      if windwidth <= 84 or #data > estimated_space_available then
        data = shorten_path(data, path_separator)
      end
    end
  end

  if self.options.file_status then
    if vim.bo.modified then
      data = data .. self.options.symbols.modified
    end
    if vim.bo.modifiable == false or vim.bo.readonly == true then
      data = data .. self.options.symbols.readonly
    end
  end
  return data
end

return M
