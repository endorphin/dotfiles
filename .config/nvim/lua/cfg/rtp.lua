-- luacheck: globals req vim T

local fn = vim.fn
local opt = vim.opt
local cmd = vim.cmd
--local b = vim.b
local g = vim.g

-- rpath improvement {{{
-- Take old rpath and exclude per-user paths from there
local oldrtp=opt.runtimepath:get()
local homedir=fn.glob("~")
oldrtp=fn.substitute(oldrtp,homedir.."/.vim","","")
oldrtp=fn.substitute(oldrtp,homedir.."/.local/share/nvim/site[^,]*","","")
oldrtp=fn.substitute(oldrtp,homedir.."/.config/nvim[^,]*","","")

oldrtp=fn.substitute(oldrtp,",,",",","g")
oldrtp=fn.substitute(oldrtp,"^,","","")
oldrtp=fn.substitute(oldrtp,",$","","")

-- Rearranging runtimepath to proper order: user-defined plugins have
-- priority over systemwide ones
local rtp=T{}
rtp:insert(homedir.."/.config/nvim,")
rtp:insert(homedir.."/.local/share/nvim/site,")
rtp:insert(homedir.."/.vim")
rtp:insert(","..oldrtp)
opt.runtimepath = rtp:concat()
opt.runtimepath = fn.substitute(opt.runtimepath, "/usr/share/nvim/runtime,", "\\0/usr/share/vim/vimfiles,", "")
opt.runtimepath = fn.substitute(opt.runtimepath, "/usr/share/nvim/site/after,",
  homedir.."/.local/share/nvim/site/after,"..homedir.."/.config/nvim/after,\\0", "")
opt.runtimepath = fn.substitute(opt.runtimepath, "/usr/share/nvim/site/after,", "\\0/usr/share/vim/vimfiles/after,", "")

local EPREFIX=os.getenv('EPREFIX')

if EPREFIX and #EPREFIX>0 then
  opt.runtimepath=fn.substitute(opt.runtimepath,",/usr",","..EPREFIX.."/usr","g")
  opt.runtimepath=fn.substitute(opt.runtimepath,",/etc",","..EPREFIX.."/etc","g")
--  g.python_host_prog=EPREFIX..g:python_host_prog
  g.python3_host_prog=EPREFIX..g.python3_host_prog
end

cmd[[runtime! ftdetect/*.vim]] -- hack to re-apply syntax things from Vim's systemwide site-dir
-- rpath }}}
