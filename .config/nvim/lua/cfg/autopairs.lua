-- luacheck: globals vim req T
local npairs = req"nvim-autopairs"
-- local Rule = req"nvim-autopairs.rule"
-- local cond = req"nvim-autopairs.conds"
-- local ts_cond = req"nvim-autopairs.ts-conds"


local plugins = _G.packer_plugins or {}
if plugins['nvim-cmp'] then
  local cmp = req"cmp"
  local cmp_autopairs = req'nvim-autopairs.completion.cmp'
  local cmp_handlers = req'nvim-autopairs.completion.handlers'
  -- add a lisp filetype (wrap my-function), FYI: Hardcoded = { "clojure", "clojurescript", "fennel", "janet" }
  -- cmp_autopairs.lisp[#cmp_autopairs.lisp+1] = "racket"
  cmp.event:on(
    'confirm_done',
    cmp_autopairs.on_confirm_done({
      filetypes = {
        -- "*" is a alias to all filetypes
        ["*"] = {
          ["("] = {
            kind = {
              cmp.lsp.CompletionItemKind.Function,
              cmp.lsp.CompletionItemKind.Method,
            },
            handler = cmp_handlers["*"]
          }
        },
        lua = {
          ["("] = {
            kind = {
              cmp.lsp.CompletionItemKind.Function,
              cmp.lsp.CompletionItemKind.Method
            },
            ---@param char string
            ---@param item item completion
            ---@param bufnr buffer number
            handler = function(char, item, bufnr)
              -- Your handler function. Inpect with print(vim.inspect{char, item, bufnr})
              print(vim.inspect{char, item, bufnr})
            end
          }
        },
        -- Disable for tex
        tex = false
      }
    })
  )
end

npairs.setup{
  check_ts = true,
  ts_config = {
    lua = {'string'},-- it will not add a pair on that treesitter node
    javascript = {'template_string'},
    java = false,-- don't check treesitter on java
  },
--  enable_check_bracket_line = false,
}


--[=[
npairs.add_rules{ -- {{{
  -- [ Spaces around parentheses ] {{{
  Rule(' ', ' ')
    :with_pair(function (opts)
      local pair = opts.line:sub(opts.col - 1, opts.col)
      return vim.tbl_contains({ '()', '[]', '{}' }, pair)
    end),
  Rule('( ', ' )')
    :with_pair(function() return false end)
    :with_move(function(opts)
      return opts.prev_char:match('.%)') ~= nil
    end)
    :use_key(')'),
  Rule('{ ', ' }')
    :with_pair(function() return false end)
    :with_move(function(opts)
      return opts.prev_char:match('.%}') ~= nil
    end)
    :use_key('}'),
  Rule('[ ', ' ]')
    :with_pair(function() return false end)
    :with_move(function(opts)
      return opts.prev_char:match('.%]') ~= nil
    end)
    :use_key(']'),
  -- }}}
  -- [ Spaces on assignment ] {{{
  Rule('=', '', {"cpp", "rust", "go", "lua"})
    :with_pair(cond.not_inside_quote())
    :with_pair(function(opts)
      local last_char = opts.line:sub(opts.col - 1, opts.col - 1)
      if last_char:match('[%w%=%s]') then
        return true
      end
      return false
    end)
    :replace_endpair(function(opts)
      local prev_2char = opts.line:sub(opts.col - 2, opts.col - 1)
      local next_char = opts.line:sub(opts.col, opts.col)
      next_char = next_char == ' ' and '' or ' '
      if prev_2char:match('%w$') then
        return '<bs> =' .. next_char
      end
      if prev_2char:match('%=$') then
        return next_char
      end
      if prev_2char:match('=') then
        return '<bs><bs>=' .. next_char
      end
      return ''
    end)
    :set_end_pair_length(0)
    :with_move(cond.none())
    :with_del(cond.none()),
  -- }}}
  Rule("%", "%", "lua")
    :with_pair(ts_cond.is_ts_node({'string','comment'})),
  Rule("$", "$", "lua")
    :with_pair(ts_cond.is_not_ts_node({'function'})),
} -- }}}
--]=]
