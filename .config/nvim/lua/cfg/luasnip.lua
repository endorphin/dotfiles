-- luacheck: globals req vim T, no max line length
local luasnip = req"luasnip"

local s = luasnip.snippet
local t = luasnip.text_node
local i = luasnip.insert_node
local f = luasnip.function_node -- luacheck: ignore

luasnip.snippets = { -- {{{
  php = { -- {{{
    s('ph', {
      t({'<?php '}), i(0), t({' ?>'})
    }),
    s('phe', {
      t({'<?= '}), i(0), t({' ?>'})
    }),
    s('phpwhil', {
      t({'<?php while('}), i(1), t({'): ?>', '\t'}),
      i(0), t({ '', '' }),
      t({'<?php endwhile; ?>'})
    }),
    s('phpf', {
      t({'<?php for('}), i(1),t({', '}),i(2),t({', '}),i(3), t({'): ?>', '\t'}),
      i(0), t({ '', '' }),
      t({'<?php endfor; ?>'})
    }),
    s('phpfe', {
      t({'<?php foreach('}), i(1),t({' as '}),i(2), t({'): ?>', '\t'}),
      i(0), t({ '', '' }),
      t({'<?php endforeach; ?>'})
    }),
    s('phpif', {
      t({'<?php if('}), i(1), t({'): ?>', '\t'}),
      i(0), t({ '', '' }),
      t({'<?php endif; ?>'})
    }),
  }, -- }}}
} -- }}}

luasnip.filetype_extend("ruby", {"rails"})
luasnip.filetype_extend("python", {"django"})

luasnip.config.set_config{
  history = true,
  updateevents = "TextChanged,TextChangedI"
}

req"luasnip.loaders.from_vscode".lazy_load()
return luasnip
