-- luacheck: globals req vim T

require 'colorizer'.setup({ -- {{{
  '*', -- Highlight all files, but customize some others.
  css = { css = true, }, -- Enable parsing rgb(...) functions in css.
  html = { css= true, names = false, } -- Disable parsing "names" like Blue or Gray
  --'!type' -- exclusion from '*'
},{ -- default settings:
  mode = 'foreground',
  names = 'false',
}) -- }}}

