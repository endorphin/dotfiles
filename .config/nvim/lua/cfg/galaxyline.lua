-- luacheck: globals vim req

-- TODO: split to 'cfg.galaxyline.*'. And maybe even moveto separate repo
-- XXX: maybe think about taking some colors from current colorcheme (luakai ATM), so port color theme to export colors

local f = req"lib.funcs"
local u = f.u

local gl = req'galaxyline'
local gls = gl.section

local gl_cond = req'galaxyline.condition'
local check_git = gl_cond.check_git_workspace
local wide_enough = gl_cond.hide_in_width
local buffer_not_empty = gl_cond.buffer_not_empty

local fileinfo = req'galaxyline.provider_fileinfo'
local gl_vcs = req'galaxyline.provider_vcs'

local devicons = req'nvim-web-devicons'

-- local gl_ex = req'galaxyline.provider_extensions'
-- local dap = req'dap'


gl.short_line_list = { -- {{{
    'LuaTree',
    'NvimTree',
    'vista',
    'dbui',
    'startify',
    'term',
    'nerdtree',
    'fugitive',
    'fugitiveblame',
    'plug',
    'plugins'
} -- }}}


local function hl(...)
  vim.api.nvim_set_hl(0,...)
end

--[[{{{
local function diagnostic_exists()
  return vim.tbl_isempty(vim.lsp.buf_get_clients(0))
end
}}}]]

local function diags(severity)
  return vim.diagnostic.get(0, {severity = severity})
end



local mvaline_colors = { -- {{{
	Scroll_100 = {ctermfg=160, ctermbg=234, cterm={bold=1,}, bg="#1c1c1c", bold=1, fg="#d70000",},
	Scroll_90  = {ctermfg=166, ctermbg=234, cterm={bold=1,}, bg="#1c1c1c", bold=1, fg="#d75f00",},
	Scroll_70  = {ctermfg=172, ctermbg=234, cterm={bold=1,}, bg="#1c1c1c", bold=1, fg="#d78700",},
	Scroll_50  = {ctermfg=178, ctermbg=234, cterm={bold=1,}, bg="#1c1c1c", bold=1, fg="#d7af00",},
	Scroll_30  = {ctermfg=184, ctermbg=234, cterm={bold=1,}, bg="#1c1c1c", bold=1, fg="#d7d700",},
	Scroll_10  = {ctermfg=190, ctermbg=234, cterm={bold=1,}, bg="#1c1c1c", bold=1, fg="#d7ff00",},

	LinePos_80 = {ctermfg=52,  ctermbg=252, cterm={bold=1,}, bg="#333333", bold=1, fg="#ff3300",},
	LinePos_70 = {ctermfg=94,  ctermbg=252, cterm={bold=1,}, bg="#333333", bold=1, fg="#e7af00",},
	LinePos_30 = {ctermfg=58,  ctermbg=252, cterm={bold=1,}, bg="#333333", bold=1, fg="#cfaf00",},
	LinePos_1  = {ctermfg=22,  ctermbg=252, cterm={bold=1,}, bg="#333333", bold=1, fg="#00bf00",},
	CurPos     = {ctermfg=234, ctermbg=252, cterm={bold=1,}, bg="#333333", bold=1, fg="#efaf00",},

	CurArrow    = {ctermfg=252, ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#333333",},
	ScrollArrow = {ctermfg=234, fg="#1c1c1c",},
--                   ctermbg=234, cterm={bold=1,}, bold=1,

	NormalMode = {ctermfg=22,  ctermbg=148, cterm={bold=1,}, bold=1, bg="#afd700", fg="#005f00",},
	NormalModeArrow = {ctermfg=148, ctermbg=234, cterm={bold=1,}, bold=1, fg="#afd700", bg="#1c1c1c",},
	NormalModeArrowPaste = {ctermfg=148, ctermbg=166, cterm={bold=1,}, bold=1, fg="#afd700", bg="#d75f00",},

	InsertMode = {ctermfg=252, ctermbg=31,  cterm={bold=1,}, bold=1, bg="#0087af", fg="#d0d0d0",},
	InsertModeArrow = {ctermfg=31,  ctermbg=234, cterm={bold=1,}, bold=1, fg="#0087af", bg="#1c1c1c",},
	InsertModeArrowPaste = {ctermfg=31,  ctermbg=166, cterm={bold=1,}, bold=1, fg="#0087af", bg="#d75f00",},

	VisualMode = {ctermfg=94,  ctermbg=214, cterm={bold=1,}, bold=1, bg="#ffaf00", fg="#875f00",},
	VisualModeArrow = {ctermfg=214, ctermbg=94,  cterm={bold=1,}, bold=1, bg="#875f00", fg="#ffaf00",},
	VisualSelMode = {ctermfg=214, ctermbg=94,  cterm={bold=1,}, bold=1, bg="#875f00", fg="#ffaf00",},
	VisualSelModeArrow = {ctermfg=94,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#875f00",},
	VisualModeArrowPaste = {ctermfg=94,  ctermbg=166, cterm={bold=1,}, bold=1, bg="#d75f00", fg="#875f00",},

	ReplaceMode = {ctermfg=252, ctermbg=160, cterm={bold=1,}, bold=1, bg="#d70000", fg="#d0d0d0",},
	ReplaceModeArrow = {ctermfg=160, ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#d70000",},
	ReplaceModeArrowPaste = {ctermfg=160, ctermbg=166, cterm={bold=1,}, bold=1, bg="#d75f00", fg="#d70000",},

	PasteMode = {ctermfg=231, ctermbg=166, cterm={bold=1,}, bold=1, bg="#d75f00", fg="#d0d0d0",},
	PasteArrow = {ctermfg=166, ctermbg=234, cterm={bold=1,}, bold=1, fg="#d75f00", bg="#1c1c1c",},
	Panel = {ctermfg=239, fg="#4a4a4a",},
--                   ctermbg=234, cterm={bold=1,}, bold=1,
	isRO = {ctermfg=196, ctermbg=234, cterm={bold=1,}, bold=0, bg="#1c1c1c", fg="#ff0000",},
	FileName = {ctermfg=244, ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#808080",},
	FileNameArrow = {ctermfg=234, fg="#1c1c1c",},
--                   ctermbg=234, cterm={bold=1,}, bold=1,
	Saved = {ctermfg=22,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#00ff00",},
	Modified = {ctermfg=9,   ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#ff0000",},

	LSP_Active = {ctermfg=22,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#00ff00",},
	LSP_Inactive = {ctermfg=9,   ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#ff0000",},

	VCSI_branch = {ctermfg=10,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#ef9f00",},
	VCSI_svn = {ctermfg=14,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#00ffee",},
	VCSI_git = {ctermfg=10,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#54ff54",},
	VCSI_hg = {ctermfg=13,  ctermbg=234, cterm={bold=1,}, bold=1, bg="#1c1c1c", fg="#ff00ae",},
} -- }}}
for k,v in pairs(mvaline_colors) do
	hl(k,v)
end

-- local nr2char = vim.fn.nr2char

local function get_icons() -- {{{
  local fallback = { -- {{{
    sep = { -- {{{
      left_heavy   = u'e0b0', -- nr2char(57520), --  .
      -- nr2char(57520+(4*1)),
      left_light   = u'e0b1', -- nr2char(57521), --  .
      -- nr2char(57521+(4*1)), --
      right_heavy  = u'e0b2', -- nr2char(57522), --  .
      -- nr2char(57522+(4*1)), --
      right_light  = u'e0b3', -- nr2char(57523), --  .
      -- nr2char(57523+(4*1)), --
    }, -- }}}
    position = { -- {{{
      lnum         = u'e0a1', -- nr2char(57505), --  .
      cnum         = u'e0a3', -- nr2char(57507), --  .
    }, -- }}}
    fileinfo = { -- {{{
     ro           = u'1f50f', -- nr2char(128271), -- 🔏
     -- ro   = nr2char(57506), --  .
     -- ro = u'f023', -- 
     saved        = u'2714', -- nr2char(10004), -- ✔ .
     -- saved   = nr2char(10003), -- ✓ .
     unsaved      = u'274c', -- nr2char(10060), -- ❌ .
     -- unsaved = u'f693', --  .
      -- unsaved  = nr2char(10007), -- ✗ .
    }, -- }}}
    other = { -- {{{
      date         = u'1f4c5', -- nr2char(8986),  -- 📅
      time         = u'231a', -- nr2char(8986),  -- ⌚
    }, -- }}}
    vcs = { -- {{{
      vcs          = u'e0a0', -- nr2char(57504), --  .
      git          = u'f1d3',   --  .
      -- git          = nr2char(177),   -- ± .
      github = u'f113', --   .
      gitlab = u'f296', --   .
      bitbucket = u'f171', --  .
      hg           = u'f0c3',  --  .
      -- hg           = nr2char(9791),  -- ☿ .
      svn          = u'f28a', --  .
      -- svn          = u'e72d', -- 
    }, -- }}}
    diff = { -- {{{
      add      = u'2795', -- ➕
      -- '   ',
      del      = u'2796', -- ➖
      -- '   ',
      mod      = u'2797', -- ➗
      -- '   ',
    }, -- }}}
    diag = { -- {{{
      error = vim.fn.sign_getdefined("DiagnosticSignError")[1].text,
      warn  = vim.fn.sign_getdefined("DiagnosticSignWarn")[1].text,
      info  = vim.fn.sign_getdefined("DiagnosticSignInfo")[1].text,
      hint  = vim.fn.sign_getdefined("DiagnosticSignHint")[1].text,
    }, -- }}}
    nl = { -- {{{
      dos          = u'e70f', --  .
      unix         = u'f17c', --  .
      mac          = u'f179', --  .
    }, -- }}}
    lsp = { -- {{{
      status   = u'f0e8', --  .
    -- lsp_status   = u'f816', --  .
    -- lsp_status   = u'f02d', --  .
    }, -- }}}
    default = "",
  } -- }}}

  local ret = setmetatable(fallback, { -- {{{
      __index = function()
        local ext = vim.g.statusline_icons
        if ext and ext.default then
          return ext.default
        else
          return fallback.default
        end
      end,
    }) -- }}}

  if vim.g.statusline_icons then
    for name,icon in pairs(vim.g.statusline_icons) do
      ret[name] = icon
    end
  end
  return ret
end -- }}}

local icons = get_icons()

local modes = { -- {{{
  n  = { n = "NORMAL"},
  no = { n = "N·OPER" },
  s  = { n = "SELECT" },
  S = { n = "S·LINE" },
  [""] = { n = "S·BLCK" },
  Rv = { n = "V·RPLCE" },
  c = { n = "COMMAND" },
  cv = { n = "VIM EX" },
  ce = { n = "EX" },
  R = {
    n = "REPLACE",
    cg = "ReplaceMode",
  },
  r = { n = "PROMPT" },
  rm = { n = "MORE" },
  ["r?"] = { n ="CONFIRM" },
  ["!"] = { n ="SHELL" },
  t = { n = "TERMINAL" },
  i = {
    n = "INSERT",
    cg = "InsertMode",
  },
  v = {
    cg = "VisualMode",
    n = "VISUAL",
  },
  V = {
    cg = "VisualMode",
    n = "V·LINE",
  },
  [""] = {
    cg = "VisualMode",
    n="V·BLCK",
  },
} -- }}}

local function get_mode_hl_g() -- {{{
  local mode = vim.fn.mode()
  local hl_g = modes[mode] and modes[mode].cg or "NormalMode"
  return hl_g
end -- }}}

local function is_man()
  return vim.bo.filetype == "man" and vim.fn.exists(":Man") == 2 or false
end
local function is_help()
  return vim.bo.filetype == "help"
end
local function is_less()
  return vim.g.loaded_less and vim.g.loaded_less == 1 or false
end

gls.left = { -- {{{
  { Mode = { -- {{{
    provider = function()
      local mode = vim.fn.mode()
      local mode_hl_g = get_mode_hl_g()
      local mode_lbl = vim.b.statusline_mode_replace or (modes[mode] and modes[mode].n or mode)

      hl('GalaxyMode',{link = mode_hl_g})
      hl('GalaxyModeSep',{link = ("%sArrow%s"):format(mode_hl_g,(vim.go.paste and "Paste" or ""))})

      return "  "..mode_lbl.." "
    end,
    -- highlight = mode_cg_t, -- only for static highlight and if don't want to use default Galaxy${BLOCKNAME}
    separator = icons.sep.left_heavy,
    separator_highlight = 'GalaxyModeSep',
  }, }, -- }}}
  { VisualMode = { -- {{{
    provider = function()
      if get_mode_hl_g():match("^V") then
        return " x " -- TODO: block sizes
      end
    end,
    highlight = 'VisualSelMode',
  },}, -- }}}
  { VisualModeArrow = { -- {{{
    provider = function()
      if get_mode_hl_g():match("^V") then
        return icons.sep.left_heavy.." "
      end
    end,
    highlight = 'VisualSelModeArrow',
  },}, -- }}}
  { PasteMode = { -- {{{
    provider = function()
      if vim.go.paste then
        return " PASTE "
      end
    end,
    highlight = 'PasteMode',
    -- condition = function() return vim.go.paste end,
    -- separator = icons.sep.left_heavy,
    -- separator_highlight = 'PasteArrow',
  },}, -- }}}
  { PasteModeArrow = { -- {{{
    provider = function()
      if vim.go.paste then
        return icons.sep.left_heavy.." "
      end
    end,
    highlight = 'PasteArrow',
  },}, -- }}}
  { FileReadOnly = { -- {{{
    provider = function()
      local icon
      if vim.bo.readonly then
        icon = " "..icons.fileinfo.ro.." "
        hl('GalaxyFileReadOnly',{link = 'isRO'})
      else
        icon = ""
      end
      return icon
    end,
    condition = function() return buffer_not_empty() and not(is_man() or is_help() or is_less()) end,
  },}, -- }}}
  { FileIcon = { -- {{{
    provider = function()
      local fname, ext = vim.fn.expand('%:t'), vim.fn.expand('%:e')
      local icon, iconhl = devicons.get_icon(fname, ext)
      if icon == nil then return '' end
      local fg = vim.fn.synIDattr(vim.fn.hlID(iconhl), 'fg')
      local bg = vim.fn.synIDattr(vim.fn.hlID('FileName'), 'bg')
      hl('GalaxyFileIcon', {bg = bg, fg = fg})
      return ' ' .. icon .. ' '
    end,
    condition = buffer_not_empty,
  },}, -- }}}
  { FileName = { -- {{{
    provider = function()
      -- if not buffer_not_empty() then return '' end
      local fname
      if is_man() then -- {{{
        local head = vim.fn.getline(1)
        local s_head=head:match("^[^ ]+")
        local w_head
        w_head = s_head or "<empty>"
        local l_head=w_head:lower()
        fname = l_head:gsub("^([^%(]+)%(([^%)]+)%)","%1.%2")
        vim.b.statusline_mode_replace = "MAN"
      elseif is_help() then
        fname = vim.fn.expand("%:t:r")
        vim.b.statusline_mode_replace = "HELP"
      elseif is_less() then
        vim.b.statusline_mode_replace = "PAGER"
      else
        if wide_enough() then
          fname = vim.fn.fnamemodify(vim.fn.expand '%', ':~:.')
        else
          fname = vim.fn.expand '%:t'
        end
      end -- }}}
      if #fname == 0 then fname = "<empty>" end
      return (" %s "):format(fname)
    end,
    highlight = 'FileName',
  },}, -- }}}
  { FileSaved = { -- {{{
    provider = function() -- {{{
      local icon
      if vim.bo.modified then
        icon = icons.fileinfo.unsaved
        hl('GalaxyFileSaved',{link = 'Modified'})
      else
        icon = icons.fileinfo.saved.." "
        hl('GalaxyFileSaved',{link = 'Saved'})
      end
      return icon.." "
    end, -- }}}
    condition = function() return buffer_not_empty() and not(is_man() or is_help() or is_less()) end,
  },}, -- }}}
  { GitSep = { -- {{{
    provider = function() return ("%s "):format(icons.sep.left_light) end,
    highlight = 'GalaxyModeSep',
    condition = check_git,
  },}, -- }}}
  { GitIcon = { -- {{{
    provider = function() return (" %s "):format(icons.vcs.git) end,
    condition = check_git,
    highlight = 'VCSI_git',
    -- separator = ("%s"):format(icons.sep.left_light),
    -- separator_highlight = 'VCSI_git',
  },}, -- }}}
  { GitBranch = { -- {{{
    provider = function() -- {{{
      local ret = gl_vcs.get_git_branch()
      return ("  %s ")
        :format(ret)
        :gsub(
          ("(%s).+")
            :format(
              ('.')
              :rep(
                vim.g.statusline_branch_length_limit or 15
              )
            ),
          '%1...'
        )
    end, -- }}} -- 'GitBranch',
    condition = check_git,
    highlight = 'VCSI_branch',
  },}, -- }}}
  { FileBlockSep = { -- {{{
    provider = function() return icons.sep.left_heavy end,
    highlight = 'FileNameArrow',
    separator = ' ', --icons.sep.left_heavy,
    separator_highlight = 'FileNameArrow',
  },}, -- }}}
  { DiffAdd = { -- {{{
    provider = 'DiffAdd',
    condition = check_git,
    icon = ("%s "):format(icons.diff.add),
    highlight = 'DiffAdd',
  },}, -- }}}
  { DiffModified = { -- {{{
    provider = 'DiffModified',
    condition = check_git,
    icon = ("%s "):format(icons.diff.mod),
    highlight = 'DiffChange',
  },}, -- }}}
  { DiffRemove = { -- {{{
    provider = 'DiffRemove',
    condition = check_git,
    icon = ("%s "):format(icons.diff.del),
    highlight = 'DiffDelete',
  },}, -- }}}
} -- }}}

gls.right = { -- {{{
--[[{{{
  { BufferIcon = {
    provider= 'BufferIcon',
  }},
}}}]]
  { TrailingWhiteSpace = { -- {{{
     provider = function() -- {{{
      local trail = vim.fn.search("\\s$", "nw")
      if trail ~= 0 then
          return ' '
      else
          return nil
      end
    end, -- }}}
    icon = '  ',
    highlight = {'red','none','bold'},
  },}, -- }}}

  { TreesitterContext = { -- {{{
    provider = function() -- {{{
      local opts = {
        indicator_size = 100,
        -- type_patterns = {'class', 'function', 'method', },
        transform_fn = function(line) return line:gsub('%s*[%[%(%{]*%s*$', '') end,
        separator = '  ',
      }
      local status = vim.fn['nvim_treesitter#statusline'](opts)
      if not status or status == '' or type(status) ~= 'string' then
          return ''
      end
      return status
        :gsub('%-%-.*', '')
        :gsub('local ','')
        .." "
    end, -- }}}
    condition = function()
      local fts = {
        'lua', 'python', 'typescript', 'typescriptreact', 'react',
        'javascript', 'javascriptreact', 'rust', 'go', 'html', 'css'
      }
      for _,ft in ipairs(fts) do
       if ft == vim.bo.filetype then
         return true
       end
     end
    end,
    icon = '   ',
    highlight = 'Keyword', -- 'DiffChange', -- 'Exception',
  },}, -- }}}
{ Debug = { -- {{{
    provider =  function()
      local status = dap and dap.status()
      if not status or status == '' then
        return ''
      end
      return  '  ' .. status
    end,
    highlight = 'DiffChange', -- TODO: don't depend on DiffChange
    separator = ' ',
  },}, -- }}}
  { -- {{{ Diagnostic
    DiagnosticHint = { -- {{{
      provider = function()
        local n = #diags'Hint'
        if n == 0 then return '' end
        return (' %s %d '):format(icons.diag.hint, n)
      end,
      highlight = 'LspDiagnosticSignHint',
    }, -- }}}
    DiagnosticInfo = { -- {{{
      provider = function()
        local n = #diags'Info'
        if n == 0 then return '' end
        return (' %s %d '):format(icons.diag.info, n)
      end,
      highlight = 'LspDiagnosticSignInformation',
    }, -- }}}
    DiagnosticWarn = { -- {{{
      provider = function()
        local n = #diags'Warn'
        if n == 0 then return '' end
        return (' %s %d '):format(icons.diag.warn, n) -- u'f071'
      end,
      highlight = 'LspDiagnosticsSignWarning',
    }, -- }}}
    DiagnosticError = { -- {{{
      provider = function()
        local n = #diags'Error'
        if n == 0 then return '' end
        return (' %s %d '):format(icons.diag.error, n) -- u'e009'
      end,
      highlight = 'LspDiagnosticsSignError',
    }, -- }}}
  }, -- }}}
  { RightBlockSep = { -- {{{
    provider = function() return ("%s"):format(icons.sep.right_heavy) end,
    highlight = 'FileNameArrow',
    separator = ' ', --icons.sep.left_heavy,
    separator_highlight = 'FileNameArrow',
  },}, -- }}}
--[[{{{
  { DiagArrow = { -- {{{
      provider = function()
        if #diags() > 0 then
          return (" %s "):format(icons.sep.right_heavy)
        end
      end,
      highlight = 'GalaxyModeSep',
    },}, -- }}}
}}}]]
  { LspStatus = { -- {{{
      provider = function()
        local connected = not vim.tbl_isempty(vim.lsp.buf_get_clients(0))
        if connected then
          hl('GalaxyLspStatus',{link = 'LSP_Active'})
        else
          hl('GalaxyLspStatus',{link = 'LSP_Inactive'})
        end
          return ("  %s  "):format(icons.lsp.status)
      end,
      -- highlight = '',
      -- separator = icons.sep.right_heavy,
      separator_highlight = 'FileNameArrow',
    },}, -- }}}
  { FileType = { -- {{{
      provider = function()
        -- if not buffer_not_empty() then return '' end
        local ft = vim.bo.filetype
        if #ft == 0  then
          ft = "none"
        end
        return (' %s '):format(ft)
      end,
      condition = buffer_not_empty,
      highlight = 'FileName',
      separator = icons.sep.right_light,
      separator_highlight = 'GalaxyModeSep',
    },}, -- }}}
  { FileFormat = { -- {{{
      provider = function()
        -- if not buffer_not_empty() then return '' end
        local icon = icons.nl[vim.bo.fileformat] or ''
        return (' %s '):format(icon)
      end,
      condition = buffer_not_empty,
      highlight = 'Scroll_10',
      separator = icons.sep.right_light,
      separator_highlight = 'GalaxyModeSep',
    },}, -- }}}
  { PC = { -- {{{
      provider = function()
        local pc = fileinfo.current_line_percent()
        local pos
        if pc == " Top " then
          pos = 0
        elseif pc == " Bot " then
          pos = 100
        else
          pos = tonumber(pc:match("(%d+)%%"))
        end
        if pos < 10 then
          hl('GalaxyPC',{link = 'Scroll_10'})
        elseif pos < 30 then
          hl('GalaxyPC',{link = 'Scroll_30'})
        elseif pos < 50 then
          hl('GalaxyPC',{link = 'Scroll_50'})
        elseif pos < 70 then
          hl('GalaxyPC',{link = 'Scroll_70'})
        elseif pos < 90 then
          hl('GalaxyPC',{link = 'Scroll_90'})
        else
          hl('GalaxyPC',{link = 'Scroll_100'})
        end
        return pc
      end,
      -- highlight = 'GalaxyMode',
      condition = buffer_not_empty,
      separator = icons.sep.right_light,
      separator_highlight = 'GalaxyModeSep',
    },}, -- }}}
  { LN = { -- {{{
      provider = {
        function()
          return (' %s%s '):format(icons.position.lnum, vim.fn.line('.'))
        end,
      },
      highlight = 'CurPos',
      condition = buffer_not_empty,
      separator = icons.sep.right_heavy,
      separator_highlight = 'CurArrow',
    },}, -- }}}
  { CN = { -- {{{
      provider = {
        function()
          local pos = vim.fn.col'.'
          if pos < 35 then
            hl('GalaxyCN',{link = 'LinePos_1'})
          elseif pos < 73 then
            hl('GalaxyCN',{link = 'LinePos_30'})
          elseif pos < 77 then
            hl('GalaxyCN',{link = 'LinePos_70'})
          else
            hl('GalaxyCN',{link = 'LinePos_80'})
          end
          return (' %s%s '):format(icons.position.cnum, vim.fn.col('.'))
        end,
      },
      condition = buffer_not_empty,
    },}, -- }}}
--[[{{{
  { ScrollBar = {
     provider = 'ScrollBar',
     highlight = 'GalaxyModeSep',
  }},
}}}]]
} -- }}}

-- {{{ ???

gls.short_line_left = gls.left
gls.short_line_right = gls.right

--[[
for k, v in pairs(gls.left) do gls.short_line_left[k] = v end
table.remove(gls.short_line_left, 5)

for k, v in pairs(gls.right) do gls.short_line_right[k] = v end
]]

-- }}}

