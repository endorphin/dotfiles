-- luacheck: globals req vim T, no max line length

local tree_cb = req'nvim-tree.config'.nvim_tree_callback

-- [ colors ] {{{
local hi = vim.api.nvim_set_hl

hi(0,"NvimTreeSpecialFile",       {fg="#FFAAFF", bold=1, italic=1})
hi(0,"NvimTreeOpenedFolderName",  {link = "Directory"})
hi(0,"NvimTreeFolderName",        {link = "Directory"})
hi(0,"NvimTreeFolderIcon",        {fg = "#00AAAA"}) -- link = "Directory"})
hi(0,"NvimTreeEmptyFolderName",   {link = "NvimTreeFolderName"})
hi(0,"NvimTreeRootFolder",        {link = "Type"})

-- }}}

-- {{{
vim.g.nvim_tree_refresh_wait = 500 -- 1000 by default, control how often the tree can be refreshed, 1000 means the tree can be refresh once per 1000ms.
vim.g.nvim_tree_width_allow_resize  = 1

--[=[-- ^^^
"If 0, do not show the icons for one of 'git' 'folder' and 'files'
"1 by default, notice that if 'files' is 1, it will only display
"if nvim-web-devicons is installed and on your runtimepath.
"if folder is 1, you can also tell folder_arrows 1 to show small arrows next to the folder icons.
"but this will not work when you set indent_markers (because of UI conflict)
--]=]--

local lsp_icons = { -- {{{
  --hint = " ",
  hint = " ",
  info = " ",
  warning = " ",
  --error = " ",
  error = " ",
} --}}}

-- }}}

-- auto_close {{{
local f = req"lib.funcs"
local au = f.au
local au2 = f.au2
local g = vim.g

au(
  "BufDelete",
  "NvimTree*",
  function()
    g.nvimtree_was_unloaded = true
  end
)

au(
  "BufWinEnter",
  '*',
  function()
    if g.nvimtree_was_unloaded and vim.api.nvim_buf_get_name(0) == "" then
      vim.cmd[[silent! quitall!]]
    end
  end
)

au2("BufEnter", {
  nested = true,
  callback = function()
    if #vim.api.nvim_list_wins() == 1 and vim.api.nvim_buf_get_name(0):match("NvimTree_") ~= nil then
      vim.cmd "silent! quitall!"
    end
  end
})
-- }}}

require'nvim-tree'.setup { -- {{{
  auto_reload_on_write    = true,
  create_in_closed_folder = false,  -- true by default, When creating files, sets the path of a file when cursor is on a closed folder to the parent folder when false, and inside the folder when true.
  disable_netrw           = false, -- disables netrw completely
  hijack_cursor           = true, -- hijack the cursor in the tree to put it at the start of the filename
  hijack_netrw            = true, -- hijack netrw window on startup
  hijack_unnamed_buffer_when_opening = false,
  ignore_buffer_on_setup  = false,
  open_on_setup           = true, -- open the tree when running this setup function
  open_on_setup_file      = false,
  open_on_tab             = true, -- opens the tree when changing/opening a new tab if the tree wasn't previously opened
  respect_buf_cwd         = true,  -- false by default, will change cwd of nvim-tree to that of new buffer's when opening nvim-tree.
  sort_by                 = "name",
  update_cwd              = true, -- updates the root directory of the tree on `DirChanged` (when your run `:cd` usually)
  view = { -- {{{
    width = 30, -- width of the window, can be either a number (columns) or a string in `%`, for left or right side placement
    height = 30, -- height of the window, can be either a number (columns) or a string in `%`, for top or bottom side placement
    hide_root_folder = false,
    side = 'left', -- side of the tree, can be one of 'left' | 'right' | 'top' | 'bottom'
    preserve_window_proportions = false,
    number = false,
    relativenumber = false,
    signcolumn = "yes",
    mappings = { -- {{{
      custom_only = false, -- custom only false will merge the list with the default mappings
                           -- if true, it will only use your list to set the mappings
      list = { -- {{{ -- list of mappings to set on the tree manually
        -- default mappings
        { key = {"<CR>", "o", "<2-LeftMouse>"}, cb = tree_cb("edit") },
        { key = {"<2-RightMouse>", "<C-]>"},    cb = tree_cb("cd") },
        { key = "<C-v>",                        cb = tree_cb("vsplit") },
        { key = "<C-x>",                        cb = tree_cb("split") },
        { key = "<C-t>",                        cb = tree_cb("tabnew") },
        { key = "<",                            cb = tree_cb("prev_sibling") },
        { key = ">",                            cb = tree_cb("next_sibling") },
        { key = "P",                            cb = tree_cb("parent_node") },
        { key = "<BS>",                         cb = tree_cb("close_node") },
        { key = "<S-CR>",                       cb = tree_cb("close_node") },
        { key = "<Tab>",                        cb = tree_cb("preview") },
        { key = "K",                            cb = tree_cb("first_sibling") },
        { key = "J",                            cb = tree_cb("last_sibling") },
        { key = "I",                            cb = tree_cb("toggle_ignored") },
        { key = "H",                            cb = tree_cb("toggle_dotfiles") },
        { key = "R",                            cb = tree_cb("refresh") },
        { key = "a",                            cb = tree_cb("create") },
        { key = "d",                            cb = tree_cb("remove") },
        { key = "r",                            cb = tree_cb("rename") },
        { key = "<C-r>",                        cb = tree_cb("full_rename") },
        { key = "x",                            cb = tree_cb("cut") },
        { key = "c",                            cb = tree_cb("copy") },
        { key = "p",                            cb = tree_cb("paste") },
        { key = "y",                            cb = tree_cb("copy_name") },
        { key = "Y",                            cb = tree_cb("copy_path") },
        { key = "gy",                           cb = tree_cb("copy_absolute_path") },
        { key = "[c",                           cb = tree_cb("prev_git_item") },
        { key = "]c",                           cb = tree_cb("next_git_item") },
        { key = "-",                            cb = tree_cb("dir_up") },
        { key = "s",                            cb = tree_cb("system_open") },
        { key = "q",                            cb = tree_cb("close") },
        { key = "g?",                           cb = tree_cb("toggle_help") },
      } -- }}}
    } -- }}}
  }, -- }}}
  renderer = { -- {{{
    add_trailing = true, -- false by default, append a trailing slash to folder names
    group_empty = true, -- false by default, compact folders that only contain a single folder into one node in the file tree
    highlight_git = true,  -- false by default, will enable file highlight for git attributes (can be used without the icons).
    highlight_opened_files = "icon", -- "none" by default, will enable folder and file icon highlight for opened files/directories. Values: "none", "icon", "name", "all"
    root_folder_modifier = ':~', -- This is the default. See :help filename-modifiers for more options
    icons = { -- {{{
      webdev_colors = true,
      symlink_arrow = ' >> ', -- defaults to ' ➛ '. used as a separator between symlinks' source and target.
      padding = ' ', -- one space by default, used for rendering the space between the icon and the filename. Use with caution, it could break rendering if you set an empty string depending on your font.
      glyphs = { -- {{{
        default =        '',
        -- default =        '',
        symlink =        '',
        git = {
          unstaged =     "✗",
          staged =       "✓",
          unmerged =     "",
          renamed =      "➡",
          untracked =    "★",
          deleted =      "",
          ignored =      "◌",
        },
        folder = {
          arrow_open = "",
          arrow_closed = "",
          default =      "",
      --    default =      "📁",
          open =         "",
      --    open =         "📂",
          empty =        "",
          empty_open =   "",
          symlink =      "",
          symlink_open = "",
        },
        -- lsp = lsp_icons,
      }, -- }}}
      show = {
        git = true,
        folder = true,
        file = true,
        folder_arrow = true, -- conflicts with renderer.indent_markers
      },
    }, -- }}}
    special_files = { --  List of filenames that gets highlighted with NvimTreeSpecialFile
      ['README.md'] = 1,
      Makefile = 1,
      MAKEFILE = 1,
    },
    indent_markers = { -- {{{ -- 0 by default, this option shows indent markers when folders are open
      enable = false,
      icons = { -- {{{
        corner = "└ ",
        edge = "│ ",
        none = "  ",
      }, -- }}}
    }, -- }}}
  }, -- }}}
  hijack_directories  = { -- hijacks new directory buffers when they are opened. {{{
    enable = true, -- enable the feature
    auto_open = false, -- allow to open the tree if it was previously closed
  }, -- }}}
  update_focused_file = { -- update the focused file on `BufEnter`, un-collapses the folders recursively until it finds the file {{{
    enable      = true, -- enables the feature
    update_cwd  = true, -- update the root directory of the tree to the one of the folder containing the file if the file is not under the current root directory
                        -- only relevant when `update_focused_file.enable` is true
    ignore_list = {}    -- list of buffer names / filetypes that will not update the cwd if the file isn't found under the current root directory
                        -- only relevant when `update_focused_file.update_cwd` is true and `update_focused_file.enable` is true
  }, -- }}}
  ignore_ft_on_setup   = {}, -- will not open on setup if the filetype is in this list
  system_open = { -- configuration options for the system open command (`s` in the tree by default) {{{
    cmd  = nil, -- the command to run this, leaving nil should work in most cases
    args = {} -- the command arguments as a list
  }, -- }}}
  diagnostics = { -- show lsp diagnostics in the signcolumn {{{
    enable = false,
    icons = lsp_icons,
  }, -- }}}
  filters = { -- {{{
  -- empty bt default
    dotfiles = true,
    custom = {
      '.git',
      'node_modules',
      '.cache',
    }
  }, -- }}}
  git = { -- {{{
    enable = true,
    ignore = true,
    timeout = 400,
  }, -- }}}
  actions = { -- {{{
    use_system_clipboard = true,
    change_dir = { -- {{{
      enable = true,
      global = false,
      restrict_above_cwd = false,
    }, -- }}}
    open_file = { -- {{{
      quit_on_open = true, -- false,
      resize_window = true, --false,
      window_picker = { -- {{{
        enable = false, -- true,
        chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
        exclude = { -- {{{
          filetype = { "notify", "packer", "qf", "diff", "fugitive", "fugitiveblame" },
          buftype = { "nofile", "terminal", "help" },
        }, -- }}}
      }, -- }}}
    }, -- }}}
  }, -- }}}
  trash = { -- {{{
    cmd = "trash",
    require_confirm = true,
  }, -- }}}
  log = { -- {{{
    enable = false,
    truncate = false,
    types = {
      all = false,
      config = false,
      copy_paste = false,
      diagnostics = false,
      git = false,
      profile = false,
    }, -- }}}
  }, -- }}}
} -- }}}


