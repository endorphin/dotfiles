-- luacheck: globals req vim T nmap, no max line length
local lsp = req"lspconfig"
local lsp_configs = req"lspconfig.configs"
-- local lsp_util = req"lspconfig.util"
local lsp_signature = req"lsp_signature"
local lsp_installer = req"nvim-lsp-installer"
local setups = {}

local srv_p = vim.fn.stdpath("data").."/lsp_servers"

-- [ Colors ] {{{
local hi = vim.api.nvim_set_hl
local sign_define = vim.fn.sign_define

hi(0, "LSPErrorSign",   { fg="#ee0000" }) --    ""
hi(0, "LSPWarningSign", { fg="#eeee00" }) --   ""
hi(0, "LSPInfoSign",    { fg="#2288ee" })  --  ""
hi(0, "LSPHintSign",    { fg="#33dd99" })  --     

-- FIXME
hi(0, "LspReferenceText",  { bg="#ffff00", fg="#ff0000", bold=1 })
hi(0, "LspReferenceRead",  { bg="#00ffff", fg="#0000ff", bold=1 })
hi(0, "LspReferenceWrite", { bg="#00ff00" })

hi(0, "LspDiagnosticsUnderlineError", {undercurl = 1,})
hi(0, "LspDiagnosticsUnderlineWarning", {undercurl = 1,})
hi(0, "LspDiagnosticsUnderlineInformation", {undercurl = 1,})
hi(0, "LspDiagnosticsUnderlineHint", {undercurl = 1,})

sign_define("DiagnosticSignError", {
  text="",
-- text="✗",
-- text="",
  numhl="LspDiagnosticsSignError",
  texthl="LspDiagnosticsSignError"
-- linehl=
})
sign_define("DiagnosticSignWarn", {
  text="",
-- text=⚠ ",
  numhl="LspDiagnosticsSignWarning",
  texthl="LspDiagnosticsSignWarning"
})
sign_define("DiagnosticSignInfo", {
  text="",
-- text="",
  numhl="LspDiagnosticsSignInformation",
  texthl="LspDiagnosticsSignInformation"
})
sign_define("DiagnosticSignHint", {
  text="",
-- text="",
-- text="",
-- text="➤",
  numhl="LspDiagnosticsSignHint",
  texthl="LspDiagnosticsSignHint"
})

hi(0,"LspDiagnosticsDefaultError", {link = "LSPErrorSign",})
hi(0,"LspDiagnosticsDefaultWarning", {link = "LSPWarningSign",})
hi(0,"LspDiagnosticsDefaultInformation", {link = "LSPInfoSign",})
hi(0,"LspDiagnosticsDefaultHint", {link = "LSPHintSign",})

hi(0,"LspDiagnosticsVirtualTextError", {link = "LSPErrorSign",})
hi(0,"LspDiagnosticsVirtualTextWarning", {link = "LSPWarningSign",})
hi(0,"LspDiagnosticsVirtualTextInformation", {link = "LSPInfoSign",})
hi(0,"LspDiagnosticsVirtualTextHint", {link = "LSPHintSign",})

-- LspDiagnosticsFloatingError
-- LspDiagnosticsFloatingWarning
-- LspDiagnosticsFloatingInformation
-- LspDiagnosticsFloatingHint

hi(0,"LspDiagnosticsSignError", {link = "LSPErrorSign",})
hi(0,"LspDiagnosticsSignWarning", {link = "LSPWarningSign",})
hi(0,"LspDiagnosticsSignInformatio", {link = "nLSPInfoSign",})
hi(0,"LspDiagnosticsSignHint", {link = "LSPHintSign",})
-- }}}


local function E(list) -- {{{
  local ret = {}
  local ls_bins = {
    ccls  = "ccls",
    gopls = "gopls",
    pylsp = "pylsp",
    html = "vscode-html-language-server",
    jsonls = "vscode-json-language-server",
    cssls = "vscode-css-language-server",
    eslint = "vscode-eslint-language-server",
    -- markdown = "vscode-markdown-language-server",
  }
  for _,srv in ipairs(list) do
    local ex = io.popen(("which %s 2>/dev/null"):format(ls_bins[srv])):read()
    if ex then
      table.insert(ret, srv)
    end
  end
  return ret
end -- }}}


if lsp_installer and lsp_installer.settings then -- {{{
  lsp_installer.setup {
    -- automatic_installation = true, -- automatically detect which servers to install (based on which servers are set up via lspconfig)
    automatic_installation = {
      exclude = E{
        "ccls",
        "gopls",

        "pylsp",

        "html",
        "cssls",
        "jsonls",
        "eslint",
--        "markdown", -- vscode markdowm server, no config yet
      },
    },
    ui = {
      icons = { -- TODO: bigger (bold) icons
        server_installed = "✓",
        server_pending = "➜",
        server_uninstalled = "✗"
      }
    }
  }
--  lsp_installer.on_server_ready(function(server)
--    local opts = setups.default
--    server:setup(opts)
--    vim.cmd [[ do User LspAttachBuffers ]]
--  end)
end -- }}}


local servers = { -- {{{
  -- "pyright", -- python
  -- "jedi_language_server", -- python
  -- "rust_analyzer",
  -- "tsserver", -- typescript
  -- "solargraph", -- ruby

  -- Can't fnally choose between this two: clangd is developed by upstream, but ccls supports cxx-highlight plugin
  -- "clangd",
  "ccls",

  "cmake",
  "dockerls",
  "gopls",
  "rls",
  "pylsp",
  "lua", -- not included in upstream lspconfig. I added it manually a bit above
  "jsonls",
  "cssls",
  "cssmodules_ls",
  "html",
  "eslint",
  "arduino_language_server",
  "yamlls",
  "ansiblels",
  "asm_lsp",
  "awk_ls",
  "diagnosticls",
  "sqlls",
  "sqls",
  "golangci_lint_ls",


  -- "efm",

  -- "grammarly",

  -- Markdown
  "marksman",
  -- "remark_ls",
  -- "prosemd_lsp",
  -- "zk",

  -- PHP
  -- "intelephsense",
  -- "phpactor",
  -- "psalm",

  -- Perl
  -- "perlnavigator",

  -- PowerShell
  -- "powershell_es",

  "stylelint_lsp",

  -- VimL
  "vimls",

  -- XML
  "lemminx",
} -- }}}

local rocks_path = ("%s/packer_hererocks/%s/share/lua/5.1"):format(vim.fn.stdpath'data',jit.version:gsub('LuaJIT ', ''))

lsp_configs.lua = { -- {{{ Custom lua-lsp server
  default_config = {
    cmd = {
      [[luajit]], [[-e]],
      table.concat{
        ([[package.path = package.path..";__ROCKS__/?.lua;__ROCKS__/?/init.lua"]]):gsub("__ROCKS__",rocks_path),
        [[require"lua-lsp.loop"()]],
      }
    };
    filetypes = {"lua", "moon"};
    root_dir = function()
      return vim.loop.cwd()
    end
    -- lsp_util.path.dirname
  };
  -- on_new_config = function(new_config) end;
  -- on_attach = function(client, bufnr) end;
} --}}}
lsp_configs.emmet_ls = { -- {{{ Custom LSP server for "emmet" (creates HTML blocks from selector line)
  default_config = {
    cmd = {
      srv_p.."/emmet_ls/node_modules/.bin/"..'emmet-ls',
      '--stdio'
    };
    filetypes = {'html', 'css', 'blade'};
    root_dir = function()
      return vim.loop.cwd()
    end;
    settings = {};
  };
} -- }}}

local function goenv() -- {{{
  local env = io.popen("go env");
  local ret = {}
  for line in env:lines() do
    local k, v = line:match([=[^([^=]+)%="(.*)"$]=])
    ret[k]=v
  end
  return ret
end -- }}}
local function gopath() -- luacheck: ignore -- {{{
  local vp = vim.g.gopath;
  if vp then
    return {GOPATH=vp}
  else
    return goenv().GOPATH
  end
end -- }}}

local function on_attach(client, bufnr) -- {{{
  -- local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end -- buffer-local map
  -- local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end -- buffer-local setopt
  local opts = { remap=false, silent=true, buffer=true, } -- mapping opts

  -- buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
  vim.bo['omnifunc'] = 'v:lua.vim.lsp.omnifunc'

  -- Mappings. -- {{{
  nmap('gD', vim.lsp.buf.declaration, opts)
  nmap('gd', vim.lsp.buf.definition, opts)
  nmap('K', vim.lsp.buf.hover, opts) -- <space>h
  nmap('gi', vim.lsp.buf.implementation, opts)
  nmap('<C-k>', vim.lsp.buf.signature_help, opts)
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, opts)
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
  nmap('<leader>wl', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, opts)
  nmap('<leader>D', vim.lsp.buf.type_definition, opts)
  nmap('<leader>rn', vim.lsp.buf.rename, opts)
  nmap('<leader>ca', vim.lsp.buf.code_action, opts)
  nmap('gr', vim.lsp.buf.references, opts)
  nmap('<leader>e', vim.diagnostic.open_float, opts)
  nmap('[d', vim.diagnostic.goto_prev, opts)
  nmap(']d', vim.diagnostic.goto_next, opts)
  nmap('<leader>q', vim.diagnostic.setloclist, opts)
  --}}}
  -- Set some keybinds conditional on server capabilities
  if client.server_capabilities.document_formatting then -- {{{
    buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
  end
  if client.server_capabilities.document_range_formatting then
    buf_set_keymap("v", "<space>f", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
  end -- }}}

  -- Set autocommands conditional on server_capabilities -- {{{
  if client.server_capabilities.document_highlight then -- TODO: move to aus in customizations
    vim.api.nvim_exec([[
      hi LspReferenceRead  gui=bold,nocombine guifg=#ee9929
      hi LspReferenceText  gui=bold,nocombine guifg=#ee1b29
      hi LspReferenceWrite gui=bold,nocombine guifg=#ee2999
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]], false)
  end -- }}}
  if lsp_signature ~= nil then -- {{{
    lsp_signature.on_attach({ -- {{{
      debug = false, -- set to true to enable debug logging
      log_path = "debug_log_file_path", -- debug log path
      verbose = false, -- show debug line number

      bind = true, -- This is mandatory, otherwise border config won't get registered.
                   -- If you want to hook lspsaga or other signature handler, pls set to false
      doc_lines = 10, -- will show two lines of comment/doc(if there are more than two lines in doc, will be truncated);
                     -- set to 0 if you DO NOT want any API comments be shown
                     -- This setting only take effect in insert mode, it does not affect signature help in normal
                     -- mode, 10 by default

      floating_window = true, -- show hint in a floating window, set to false for virtual text only mode

      floating_window_above_cur_line = true, -- try to place the floating above the current line when possible Note:
      -- will set to true when fully tested, set to false will use whichever side has more space
      -- this setting will be helpful if you do not want the PUM and floating win overlap
      fix_pos = true,  -- set to true, the floating window will not auto-close until finish all parameters
      hint_enable = false, -- true if no floating -- virtual hint enable
      --hint_prefix = "🐼 ",  -- Panda for parameter
      hint_prefix = " ",  -- Panda for parameter
      hint_scheme = "String",
      use_lspsaga = false,  -- set to true if you want to use lspsaga popup
      -- hi_parameter = "LspSignatureActiveParameter", -- how your parameter will be highlight
      hi_parameter = "IncSearch", -- ModeMsg -- how your parameter will be highlight
      max_height = 12, -- max height of signature floating_window, if content is more than max_height, you can scroll down
                       -- to view the hiding contents
      max_width = 120, -- max_width of signature floating_window, line will be wrapped if exceed max_width
      handler_opts = {
        border = "rounded"   -- double, rounded, single, shadow, none
      },

      always_trigger = true, -- sometime show signature on new line or in middle of parameter can be confusing, set it to false for #58

      auto_close_after = nil, -- autoclose signature float win after x sec, disabled if nil.
      extra_trigger_chars = {}, -- Array of extra characters that will trigger signature completion, e.g., {"(", ","}
      zindex = 200, -- by default it will be on top of all floating windows, set to <= 50 send it to bottom

      padding = '', -- character to pad on left and right of signature can be ' ', or '|'  etc

      transparency = nil, -- disabled by default, allow floating win transparent value 1~100
      shadow_blend = 36, -- if you using shadow as border use this set the opacity
      shadow_guibg = 'Black', -- if you using shadow as border use this set the color e.g. 'Green' or '#121315'
      timer_interval = 200, -- default timer check interval set to lower value if you want to reduce latency
      toggle_key = nil -- <M-f> -- toggle signature on and off in insert mode,  e.g. toggle_key = '<M-x>'
    }, bufnr) -- }}}
  end -- }}}
end -- }}}

-- {{{ capabilities
local capabilities = req('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
capabilities.textDocument.completion.completionItem.documentationFormat = { -- {{{
  "markdown",
  "plaintext"
} -- }}}
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.preselectSupport = true
capabilities.textDocument.completion.completionItem.insertReplaceSupport = true
capabilities.textDocument.completion.completionItem.labelDetailsSupport = true
capabilities.textDocument.completion.completionItem.deprecatedSupport = true
capabilities.textDocument.completion.completionItem.commitCharactersSupport = true
capabilities.textDocument.completion.completionItem.tagSupport = { -- {{{
  valueSet = { 1 }
} -- }}}
capabilities.textDocument.completion.completionItem.resolveSupport = { -- {{{
  properties = { -- {{{
    "documentation",
    "detail",
    "additionalTextEdits"
  } -- }}}
} -- }}}
-- }}}

setups.default = { -- {{{
  on_attach = on_attach,
  capabilities = capabilities,
  flags = {
    debounce_text_changes = 150 -- ?
  },
  --    root_dir = vim.loop.cwd,
} -- }}}

setups.gopls = { -- {{{
--{{{   cmd = {
--      --("%s/bin/gopls"):format(goenv().GOROOT),
--      ("%s/bin/gopls"):format(gopath()),
--      "serve",
--}}}   },
  settings = {
    gopls = {
      analyses = {
        unusedresults = true,
        unusedparams = true,
        composites = true,
      },
      staticcheck = true,
    },
  },
 capabilities = capabilities,
 on_attach = on_attach,
} -- }}}
setups.ccls = { -- {{{
  init_options = {
    highlight = {
      lsRanges = true,
    },
  },
  on_attach = on_attach,
  capabilities = capabilities,
  } -- }}}


-- Schemastore {{{
local schemastore_schemas;

local plugins = _G.packer_plugins or {}
if plugins['schemastore.nvim'] then
  schemastore_schemas = req'schemastore'.json.schemas()
end
-- }}}

setups.jsonls = { -- {{{
  settings = {
    json = {
      schemas = schemastore_schemas,
      validate = { enable = true },
    },
  },
} -- }}}

setups.yamlls = { -- {{{
  settings = {
    yaml = {
      schemas = schemastore_schemas,
      validate = { enable = true },
    },
  },
} -- }}}

setups.ansiblels = { -- {{{
  settings = {
    ansible = {
      ansible = {
        useFullyQualifiedCollectionNames = true,
      }
    }
  }
} -- }}}

setups.pylsp = { -- {{{
  settings = {
    pylsp = {
      configurationSources = { "pyflake8" },
      plugins = {
        flake8 = {
          enabled = true,
          exclude = { '.git', '__pycache__' },
          -- ignore = {"E501"},
          maxComplexity = 10,
        },
        pycodestyle = { enabled = false, },
        pyflakes = { enabled = false, },
        mccabe = { enabled = false, },
      },
    },
  },
} -- }}}

--[[ {{{
lsp.util.default_config = vim.tbl_extend(
  "force", lsp.util.default_config, {log_level = vim.lsp.protocol.MessageType.Debug}
)
--]] --}}}

vim.lsp.handlers["textDocument/publishDiagnostics"] = -- {{{
  vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics,
    {
      signs = true,
      underline = true,
      update_in_insert = true -- false -- update diagnostics insert mode
    }
  ) -- }}}

--[[{{{ custom border
local border = {
    {"╭", "FloatBorder"},
    {"─", "FloatBorder"},
    {"╮", "FloatBorder"},
    {"│", "FloatBorder"},
    {"╯", "FloatBorder"},
    {"─", "FloatBorder"},
    {"╰", "FloatBorder"},
    {"│", "FloatBorder"}
}
--}}}]]

vim.lsp.handlers["textDocument/hover"] = -- {{{
  vim.lsp.with(vim.lsp.handlers.hover, {
    border = "single",
    -- focusable = false,
  })
-- }}}

vim.lsp.handlers["textDocument/signatureHelp"] = -- {{{
  vim.lsp.with(vim.lsp.handlers.signature_help, {
    border = "single",
    -- focusable = false,
  })
-- }}}

local function goto_definition(split_cmd) -- {{{
  local util = vim.lsp.util
  local log = req"vim.lsp.log"
  local api = vim.api

  -- note, this handler style is for neovim 0.5.1/0.6, if on 0.5, call with function(_, method, result)
  local handler = function(_, result, ctx)
    if result == nil or vim.tbl_isempty(result) then
      local _ = log.info() and log.info(ctx.method, "No location found")
      return nil
    end

    if split_cmd then
      vim.cmd(split_cmd)
    end

    if vim.tbl_islist(result) then
      util.jump_to_location(result[1])

      if #result > 1 then
        util.set_qflist(util.locations_to_items(result))
        api.nvim_command("copen")
        api.nvim_command("wincmd p")
      end
    else
      util.jump_to_location(result)
    end
  end

  return handler
end -- }}}

vim.lsp.handlers["textDocument/definition"] = goto_definition('split')

for _, srv in ipairs(servers) do
  lsp[srv].setup(setups[srv] or setups.default)
end

return {lsp = lsp, defaults = setups.default}
