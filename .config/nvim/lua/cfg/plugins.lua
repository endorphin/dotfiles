-- luacheck: globals req vim T map

--[[-- -- {{{ Current defaults of packer init:
{
  ensure_dependencies   = true, -- Should packer install plugin dependencies?
  package_root   = util.join_paths(vim.fn.stdpath('data'), 'site', 'pack'),
  compile_path = util.join_paths(vim.fn.stdpath('config'), 'plugin', 'packer_compiled.lua'),
  plugin_package = 'packer', -- The default package for plugins
  max_jobs = nil, -- Limit the number of simultaneous jobs. nil means no limit
  auto_clean = true, -- During sync(), remove unused plugins
  compile_on_sync = true, -- During sync(), run packer.compile()
  disable_commands = false, -- Disable creating commands
  opt_default = false, -- Default to using opt (as opposed to start) plugins
  transitive_opt = true, -- Make dependencies of opt plugins also opt by default
  transitive_disable = true, -- Automatically disable dependencies of disabled plugins
  auto_reload_compiled = true, -- Automatically reload the compiled file after creating it.
  git = {
    cmd = 'git', -- The base command for git operations
    subcommands = { -- Format strings for git subcommands
      update         = 'pull --ff-only --progress --rebase=false',
      install        = 'clone --depth %i --no-single-branch --progress',
      fetch          = 'fetch --depth 999999 --progress',
      checkout       = 'checkout %s --',
      update_branch  = 'merge --ff-only @{u}',
      current_branch = 'branch --show-current',
      diff           = 'log --color=never --pretty=format:FMT --no-show-signature HEAD@{1}...HEAD',
      diff_fmt       = '%%h %%s (%%cr)',
      get_rev        = 'rev-parse --short HEAD',
      get_msg        = 'log --color=never --pretty=format:FMT --no-show-signature HEAD -n 1',
      submodules     = 'submodule update --init --recursive --progress'
    },
    depth = 1, -- Git clone depth
    clone_timeout = 60, -- Timeout, in seconds, for git clones
    default_url_format = 'https://github.com/%s' -- Lua format string used for "aaa/bbb" style plugins
  },
  display = {
    non_interactive = false, -- If true, disable display windows for all operations
    open_fn  = nil, -- An optional function to open a window for packer's display
    open_cmd = '65vnew \\[packer\\]', -- An optional command to open a window for packer's display
    working_sym = '⟳', -- The symbol for a plugin being installed/updated
    error_sym = '✗', -- The symbol for a plugin with an error in installation/updating
    done_sym = '✓', -- The symbol for a plugin which has completed installation/updating
    removed_sym = '-', -- The symbol for an unused plugin which was removed
    moved_sym = '→', -- The symbol for a plugin which was moved (e.g. from opt to start)
    header_sym = '━', -- The symbol for the header line in packer's display
    show_all_info = true, -- Should packer show all update details automatically?
    prompt_border = 'double', -- Border style of prompt popups.
    keybindings = { -- Keybindings for the display window
      quit = 'q',
      toggle_info = '<CR>',
      diff = 'd',
      prompt_revert = 'r',
    }
  },
  luarocks = {
    python_cmd = 'python' -- Set the python command to use for running hererocks
  },
  log = { level = 'warn' }, -- The default print log level. One of: "trace", "debug", "info", "warn", "error", "fatal".
  profile = {
    enable = false,
    threshold = 1, -- integer in milliseconds, plugins which load faster than this won't be shown in profile output
  }
}

--]]-- -- }}}

local pkr = req"packer"
local packer_compiled_path = pkr.config.package_root
          ..[[/packer-compiled/start/packer-compiled/plugin/packer_compiled.lua]]

pkr.init{ -- {{{
  display = {
    open_fn = function()
      return req("packer.util").float{ border = "single" }
    end,
    title = "Packer",
    --done_sym = "",
    -- "✔️"
    -- "☑️"
    done_sym = "✅",
    error_syn = "❌",
    keybindings = { toggle_info = "o" },
    --non_interactive = true,
  },
  git = {
    clone_timeout = 60 -- Timeout, in seconds, for git clones
  },
  luarocks = {
    python_cmd = os.getenv"PYTHON" or "python",
    rocks_path = vim.fn.stdpath"data".."/packer_hererocks",
  },
 compile_path = packer_compiled_path
} -- }}}

return req('packer').startup({function(use) -- {{{
 use { 'lewis6991/impatient.nvim', -- {{{ (speed up loading, improvements for `require`, use of packer cache)
   config = function() req'impatient' end
 } -- }}}
  use { 'dstein64/vim-startuptime' }
  use { "wbthomason/packer.nvim", -- {{{
    event = "VimEnter",
  } -- }}}

  -- [[ global stuff (redefines, and so on) ]] -- {{{
  use { 'rcarriga/nvim-notify', --{{{
    config = function() -- {{{
      local n = req"notify"
--      vim.notify = function() end
      vim.notify = n
      --.async
      n.setup({ -- {{{
        -- Animation style (see below for details)
        stages = "static", --"fade_in_slide_out",

        -- Function called when a new window is opened, use for changing win settings/config
        on_open = nil,

        -- Function called when a window is closed
        on_close = nil,

        -- Render function for notifications. See notify-render()
        render = "default",

        -- Default timeout for notifications
        timeout = 5000,

        -- For stages that change opacity this is treated as the highlight behind the window
        -- Set this to either a highlight group or an RGB hex value e.g. "#000000"
        background_colour = "Normal",

        -- Minimum width for notification windows
        minimum_width = 50,

        -- Icons for the different levels
        icons = {
          ERROR = "",
          WARN = "",
          INFO = "",
          DEBUG = "",
          TRACE = "✎",
        },
      }) -- }}}
      vim.cmd[[hi Normal guibg=none]]
    end, -- }}}
    requires = {
      -- [[ Plenary.nvim ]] async and other yummy things.
      -- Actually it is mostly dependency for others, and still not use it myself in my scripts yet
      'nvim-lua/plenary.nvim'
    },
--    after = "plenary.nvim",
  } -- }}}
  -- use { 'nvim-lua/plenary.nvim' }
  -- }}}

  -- [[ LSP && diagnostics ]] -- {{{
  use { 'neovim/nvim-lspconfig', -- {{{
    config = function()
      require "cfg.lsp"
    end,
    rocks = {'lua-lsp', server = 'https://luarocks.org/m/alpha-llc'}
  } -- }}}
  use { 'williamboman/nvim-lsp-installer' }
  use { 'ray-x/lsp_signature.nvim' }
  use { 'jackguo380/vim-lsp-cxx-highlight', after = 'nvim-lspconfig' }
--[[{{{ DAP (debugging) -- TODO
  use { "rcarriga/nvim-dap-ui", -- {{{
    -- module = "dapui",
    config = function()
      req"cfg.dap.ui"
    end,
    requires = { -- {{{
      { "mfussenegger/nvim-dap", -- {{{
        -- module = "dap",
        config = function()
          req"cfg.dap"
        end
      } -- }}}
    } -- }}}
  } -- }}}
--}}}]]
  use {
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons",
    config = function()
      req"cfg.trouble"
    end
  }
  use { 'b0o/schemastore.nvim',
    requires = 'neovim/nvim-lspconfig', -- as I use requiring in lsp config
  }
  use { "simrat39/symbols-outline.nvim", -- {{{
    config = function() -- {{{
      req"cfg.symbols-outline"
    end, -- }}}
    cmd = { "SymbolsOutline", "SymbolsOutlineOpen", "SymbolsOutlineClose" }
  } -- }}}
  --[[{{{ null-ls
  use { "jose-elias-alvarez/null-ls.nvim", -- {{{
    config = function() -- {{{
      local ls = req"null-ls"
      ls.setup{ -- {{{
        -- you must define at least one source for the plugin to work
        sources = {
          ls.builtins.formatting.stylua,
          ls.builtins.diagnostics.eslint,
          ls.builtins.completion.spell,
        },
      } -- }}}
    end, -- }}}
    requires = {
      "nvim-lua/plenary.nvim",
    },
    ft = { -- {{{
      "lua", "javascript", "css"
    } -- }}}
  } -- }}}
  --}}}]]
  -- use { 'msva/nvim-lspconfig-addon' }
  -- use { 'tzachar/cmp-tabnine', run='./install.sh', requires = 'hrsh7th/nvim-cmp' }

  use { -- {{{
    "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
    config = function()
      req"cfg.lsp.lines"
    end,
  } -- }}}

  -- }}}


  -- [[ General usability ]] -- {{{
  -- use { 'chrisbra/Colorizer' } -- Replaced by nvim-colorizer
--[[ Replaced by hexokinase {{{
  use { 'norcalli/nvim-colorizer.lua', -- {{{
    config = function()
      require"cfg.colorizer"
    end
  } -- }}}
 }}}]]

  -- use { 'nacitar/terminalkeys.vim' }

  use { 'RRethy/vim-hexokinase', -- {{{
    run = 'make',
--    cmd = "HexokinaseToggle",
    setup = function()
      req"cfg.hexokinase"
    end,
  } -- }}}

  use { 'kyazdani42/nvim-tree.lua', -- {{{
    requires = { 'kyazdani42/nvim-web-devicons' },
    config = function() req"cfg.nvim-tree" end,
    -- cmd = {"NvimTreeRefresh", "NvimTreeToggle"},
  } -- }}}
  --[[{{{
  use {'tamago324/lir.nvim', -- {{{ -- Simple file manager
    requires = { -- {{{
      {'kyazdani42/nvim-web-devicons'},
      {'tamago324/lir-git-status.nvim'},
    }, -- }}}
  } -- }}}
  use { 'scrooloose/nerdtree', -- {{{
    requires = { 'ryanoasis/vim-devicons' },
    cmd = { "NERDTree", "NERDTreeToggle", "NERDTreeRefreshRoot" },
  } -- }}}
  --}}}]]
  use { 'wincent/loupe' }
  -- use { 'tpope/vim-commentary' }
  use { 'b3nj5m1n/kommentary', -- {{{
    config = function()
      req'kommentary.config'.use_extended_mappings()
    end,
  } -- }}}
  use { 'godlygeek/tabular' }
--[[{{{
  use { 'Shougo/echodoc.vim', -- {{{
    config = function()
      vim.g['echodoc#enable_at_startup'] = 1
      vim.g['echodoc#type'] = 'floating'
      vim.g['echodoc#events'] = {}
      -- 'CompleteDone', 'CursorMovedI', 'CursorHoldI', 'CursorMoved', 'CursorHold', 'BufEnter'
    end
  } -- }}}
--}}}]]
  -- }}}

  -- [[ Autocompletion ]] -- {{{
      use { 'L3MON4D3/LuaSnip', --{{{
        config = function()
          req"cfg.luasnip"
        end,
        after = 'friendly-snippets',
        requires = { 'rafamadriz/friendly-snippets' }
      } --}}}
      -- {{{
      -- use { 'honza/vim-snippets' }
      -- use { 'quangnguyen30192/cmp-nvim-ultisnips' }
      -- use { 'SirVer/ultisnips' }
      -- }}}
  use { 'hrsh7th/nvim-cmp', -- {{{
    config = function() -- {{{
      require"cfg.cmp"
    end, -- }}}
    after = { "friendly-snippets", "LuaSnip", "lspkind-nvim", "cmp-nvim-lsp" },
    requires = { -- {{{
      { 'hrsh7th/cmp-nvim-lsp' },
      -- { 'hrsh7th/cmp-nvim-lsp', after = 'nvim-cmp' },
      { 'hrsh7th/cmp-nvim-lsp-document-symbol', after = 'cmp-nvim-lsp' },
--      { 'hrsh7th/cmp-nvim-lsp-signature-help', after = 'cmp-nvim-lsp' },
      -- { 'dmitmel/cmp-cmdline-history' },
      -- { 'tzachar/cmp-fuzzy-buffer' },
      -- { 'tzachar/cmp-fuzzy-path' },
      { 'hrsh7th/cmp-nvim-lua', after = 'nvim-cmp' },
      { 'hrsh7th/cmp-calc', after = 'nvim-cmp' },
      { 'hrsh7th/cmp-cmdline', after = 'nvim-cmp' },
      { 'hrsh7th/cmp-emoji', after = 'nvim-cmp' },
      { 'hrsh7th/cmp-path', after = 'nvim-cmp' },
      { 'saadparwaiz1/cmp_luasnip', after = 'nvim-cmp' },
      { 'petertriho/cmp-git', requires = { 'nvim-lua/plenary.nvim' }, after = 'nvim-cmp', config = 'req"cfg.cmp.git"' },
      { 'hrsh7th/cmp-buffer' },
      { 'tamago324/cmp-zsh', after = 'nvim-cmp', config = 'require"cfg.cmp.zsh"' },
      { 'andersevenrud/cmp-tmux', after = 'nvim-cmp' },
      -- { 'lukas-reineke/cmp-rg', after = 'nvim-cmp' },
      { 'lukas-reineke/cmp-under-comparator', },
      { 'kdheepak/cmp-latex-symbols', after = 'nvim-cmp' },

      -- { 'dcampos/nvim-snippy' },

      { 'onsails/lspkind-nvim' },
      -- { 'onsails/diaglist.nvim' },
      -- { 'Shougo/deol.nvim' },
    }, -- }}}
  } -- }}}
  -- }}}

  -- [[ Bars (statusline and tabline) ]] -- {{{
  use { 'romgrk/barbar.nvim', -- {{{
    setup = function()
      req"cfg.barbar"
    end,
  }
  -- }}}
  -- use { 'jlanzarotta/bufexplorer' }
  use { 'kyazdani42/nvim-web-devicons', -- {{{
    config = function()
      require'nvim-web-devicons'.setup{ -- {{{
        --[[ override {{{
         override = {
           zsh = {
             icon = "",
             color = "#428850",
             name = "Zsh" -- DevIcon will be appended to `name`
           }
         };
        --]] -- }}}
        -- globally enable default icons (default to false)
        -- will get overriden by `get_icons` option
        default = true;
      } --}}}
    end,
  } -- }}}
  --use { 'msva/hydroline.vim' }
  use { 'glepnir/galaxyline.nvim', -- {{{
    branch = 'main',
    after = "nvim-web-devicons",
    config = function() req"cfg.galaxyline" end
  } -- }}}
--[[
  use { 'msva/mvaline.lua.nvim', -- {{{
      config = function()
        req"mvaline".patch_expand_in_titlestring()
      end,
      rocks = {'lua-resty-fileinfo'},
  } -- }}}
 ]]
--[[{{{
  use { 'nvim-lualine/lualine.nvim', -- {{{
    requires = { 'kyazdani42/nvim-web-devicons' },
    after = 'nvim-web-devicons',
    config = function() -- {{{
      req"cfg.lualine"
    end, -- }}}
  } -- }}}
--}}}]]
  --[[{{{
  use { 'akinsho/nvim-bufferline.lua', -- {{{
    config = function() req"cfg.bufferline") end,
    event = "BufRead"
  } -- }}}
  use { 'Avimitin/neovim-deus', -- {{{
    -- neovim color theme
    after = 'Shatur/neovim-ayu',
  } -- }}}
  --[=[ Tabline {{{
  use { 'kdheepak/tabline.nvim', -- {{{
    config = function() -- {{{
      require'tabline'.setup { -- {{{
        enable = true,
        options = { -- {{{
          section_separators = {'', ''},
          component_separators = {'', ''},
          max_bufferline_percent = 66,
          show_tabs_always = true,
          show_devicons = true,
          show_bufnr = false,
          show_filename_only = true,
        } -- }}}
      } -- }}}
      vim.cmd"
        set guioptions-=e
        set sessionoptions+=tabpages,globals
      "
    end, -- }}}
    requires = { {'hoob3rt/lualine.nvim'}, {'kyazdani42/nvim-web-devicons', opt = true} }
  } -- }}}
  --]=] --}}}
  --}}}]]

  -- }}}

  -- [[ Syntaxes and ft-related stuff ]] {{{
  use { 'jamessan/vim-gnupg' }
  use { 'chrisbra/csv.vim', -- {{{
    ft = { "csv" },
    config = function() -- {{{
      local hi = vim.api.nvim_set_hl
      hi(0,"CSVColumnOdd", {link = "String",})
      hi(0,"CSVColumnEven", {link = "Statement",})
      hi(0,"CSVColumnHeaderOdd", {link = "CSVColumnOdd",})
      hi(0,"CSVColumnHeaderEven", {link = "CSVColumnEven",})

      --	highlight CSVDelimiter guifg=#aa0000 guibg=bg
      --	hi link CSVDelimiter Todo
    end -- }}}
  } -- }}}
  -- use { 'dbeniamine/cheat.sh-vim' }
  use { 'chrisbra/vim-sh-indent', -- {{{
    -- until both vims fetch updates to their bundle
--    ft = { "sh", "bash", "zsh", "ebuild", "gentoo-conf-d", "gentoo-init-d", "gentoo-env-d" },
  } -- }}}
  use { 'PProvost/vim-ps1', -- {{{
    ft = { "ps1", "psm1", "psd1", "ps1xml", "pssc", "psrc", "cdxml" }
  } -- }}}
  use { 'msva/tdesktop-colorscheme.vim' } --, ft = { "tdesktop-theme" } }
  use { 'gentoo/gentoo-syntax', -- {{{
    config = function()
      vim.cmd[[
        packadd gentoo-syntax
        runtime ftdetect/gentoo.vim
      ]]
    end,
--[[
    ft = {
      "ebuild", "gentoo-changelog", "gentoo-conf-d", "gentoo-env-d",
      "gentoo-init-d", "gentoo-make-conf", "gentoo-metadata",
      "gentoo-package-keywords", "gentoo-package-license",
      "gentoo-package-mask", "gentoo-package-properties",
      "gentoo-package-use", "gentoo-use-desc"
    }
]]--
  } -- }}}
  use { 'chr4/nginx.vim', ft = { "nginx" } }
  -- or --
  -- use { 'spacewander/openresty-vim' }
  use { 'robbles/logstash.vim' }
  use { 'lervag/vimtex', -- {{{
    ft = { "tex", "latex" },
    config = function()
      vim.g.vimtex_view_general_viewer = 'okular'
      vim.g.vimtex_view_general_options = '--unique file:@pdf\\#src:@line@tex'
      vim.g.vimtex_view_general_options_latexmk = '--unique'
    end
  } -- }}}
  use { 'alpaca-tc/beautify.vim', -- {{{
    ft = { "html", "css", "js" },
    cmd = "Beautify",
  } -- }}}
  use { 'tmux-plugins/vim-tmux' }
  use { 'embear/vim-localvimrc' }
  use { 'sheerun/vim-polyglot' } -- takes too much time in `startuptime` test
  use { 'editorconfig/editorconfig-vim' }
--[[  use { 'Shougo/context_filetype.vim', -- {{{
    setup = function()
      vim.g['context_filetype#filetypes'] = {
        perl6 = {
          {
            filetype = 'pir',
            start = 'Q:PIR\\s*{',
            ['end']  = '}',
          }
        },
        vim = {
          {
            filetype = 'python',
            start = '^\\s*python <<\\s*\\(\\h\\w*\\)',
            ['end'] = '^\\1'
          },
          {
            filetype = 'lua',
            start = '^\\s*lua <<\\s*\\(\\h\\w*\\)',
            ['end'] = '^\\1'
          },
        },
      }
      vim.g['context_filetype#same_filetypes'] = {
        c = 'cpp,d', -- In c buffers, completes from cpp and d buffers.
        cpp = 'c',
        gitconfig = '_', -- In gitconfig buffers, completes from all buffers.
        ['_'] = '_', -- In default, completes from all buffers.
      }
      vim.g['context_filetype#ignore_composite_filetypes'] = {
          ['ruby.spec'] = 'ruby',
      }
    end,
  } -- }}}]]
  use { 'arzg/vim-rust-syntax-ext', ft="rust" } --- rust: syntax extension
  use { 'simrat39/rust-tools.nvim', -- {{{ --- rust: loads of tools
    after = "nvim-lspconfig",
    config = function() req"cfg.rust" end,
    ft = "rust",
  } -- }}}
  use { 'rust-lang/rust.vim', -- {{{
    after = "rust-tools.nvim",
    setup = function()
        vim.g.rust_clip_command = 'xclip -selection clipboard'
    end,
    ft = "rust"
  } -- }}}
  use { 'fatih/vim-go', -- {{{
    config = function() req"cfg.vim-go" end,
    ft = {"go"},
  } -- }}}
  use {'rhysd/vim-clang-format', ft = {'cpp', 'c', 'h', 'hpp'}} -- CPP
  use { 'plasticboy/vim-markdown', -- {{{
    ft = {"markdown"},
  } -- }}}
  use { 'iamcco/markdown-preview.nvim', -- {{{
    run = function() vim.fn['mkdp#util#install']() end,
    config = function() req"cfg.markdown-preview" end,
    ft = {"markdown"},
  } -- }}}
  use {'mzlogin/vim-markdown-toc', cmd = {'GenTocGFM'}}

  -- [[ Tree-Sitter ]] {{{
  use { 'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    config = function() -- {{{
      req"cfg.tree-sitter"
    end, -- }}}
    requires = { -- {{{
      { 'nvim-treesitter/nvim-treesitter-refactor' },
      { 'nvim-treesitter/nvim-treesitter-textobjects' },
      { 'RRethy/nvim-treesitter-textsubjects' },
      { 'nvim-treesitter/playground', cmd = 'TSPlaygroundToggle', after = 'nvim-treesitter' },
      { 'p00f/nvim-ts-rainbow', after = 'nvim-treesitter' },
    }, -- }}}
  } -- }}}
  -- use {"andrejlevkovitch/vim-lua-format", ft = {"lua"}}
  -- }}}

  -- [[ ColorSchemes ]] {{{
    -- use { 'msva/molokai' }
    use { 'msva/luakai' }
  -- }}}

  -- [[ Autopairs, alignment, movement and similar stuff ]] {{{
  use { 'windwp/nvim-autopairs', -- {{{
    after = {"nvim-cmp", "nvim-treesitter"},
    config = function() --{{{
      req"cfg.autopairs"
    end, -- }}}
  } --}}}
  use { 'ur4ltz/surround.nvim', -- {{{
    config = function() -- {{{
      req"surround".setup{ -- {{{
        mappings_style = "sandwich", -- "surround",
        prefix = "l", -- "s" is taken by lightspeed 🤷
        pairs = { -- {{{
          nestable = { -- {{{
            b = { "(", ")" },
            s = { "[", "]" },
            B = { "{", "}" },
            a = { "<", ">" }
            }, -- }}}
          linear = { -- {{{
            q = { "'", "'" },
            t = { "`", "`" },
            d = { '"', '"' }
          }, -- }}}
        }, -- }}}
        context_offset = 100,
        load_autogroups = false,
        load_keymaps = true, -- TODO: false
        map_insert_mode = true, -- TODO: false
        qoutes = {"'", '"', '`'},
        brackets = {"(", '{', '[', '<'},
        space_on_closing_char = false,
        space_on_alias = false,
      } -- }}}
    end, -- }}}
  } -- }}}
  use { 'pechorin/any-jump.vim', -- {{{
    setup = function()
      vim.g.any_jump_window_width_ratio = 0.8
      vim.g.any_jump_window_height_ratio = 0.9
      vim.g.any_jump_disable_default_keybindings = 1
    end,
    cmd = {'AnyJump', 'AnyJumpBack'},
  } -- }}}
  use { 'beauwilliams/focus.nvim', -- {{{
    cmd = {"FocusSplitNicely", "FocusSplitCycle"},
    module = "focus",
    config = function()
      req"focus".setup({hybridnumber = true, bufnew = true})
      map("n", "<C-Space><left>", ":FocusSplitLeft<CR>")
      map("n", "<C-Space><right>", ":FocusSplitRight<CR>")
      map("n", "<C-Space><down>", ":FocusSplitDown<CR>")
      map("n", "<C-Space><up>", ":FocusSplitUp<CR>")
      map("n", "<C-Space>s", ":FocusSplitNicely<CR>")
    end
  } -- }}}
  use { "ggandor/lightspeed.nvim", -- {{{
    requires = { 'tpope/vim-repeat' },
    --[[{{{
    config = function()
      req'lightspeed'.setup { -- {{{
        ignore_case = false,
        exit_after_idle_msecs = { unlabeled = 1000, labeled = nil },
        --- s/x ---
        jump_to_unique_chars = { safety_timeout = 400 },
        match_only_the_start_of_same_char_seqs = true,
        force_beacons_into_match_width = false,
        -- Display characters in a custom way in the highlighted matches.
        substitute_chars = { ['\r'] = '¬', },
        -- Leaving the appropriate list empty effectively disables "smart" mode,
        -- and forces auto-jump to be on or off.
        safe_labels = { . . . },
        labels = { . . . },
        -- These keys are captured directly by the plugin at runtime.
        special_keys = {
          next_match_group = '<space>',
          prev_match_group = '<tab>',
        },
        --- f/t ---
        limit_ft_matches = 4,
        repeat_ft_with_target_char = false,
      } -- }}}
    end
    --}}}]]
  } -- }}}
  use {'junegunn/vim-easy-align', cmd = 'EasyAlign'} -- align
  use { "edluffy/specs.nvim", -- {{{
    config = function() --{{{
      req"cfg.specs"
    end, -- }}}
  } -- }}}
  use {'mg979/vim-visual-multi', event = "InsertEnter", branch = 'master'} -- multi cursor
  --[[{{{ neoclip (doesnt work atm)
  use { "AckslD/nvim-neoclip.lua", -- {{{
    cmd = {'ClipRec', 'ClipView'},
    config = function()
      req'neoclip'.setup()
      -- req'telescope'.load_extension('neoclip')
    end
  } -- }}}
  --}}}]]
  --[[{{{ Telescop (broken atm)
  use { 'nvim-telescope/telescope.nvim', -- {{{
    requires = {'nvim-lua/popup.nvim', 'nvim-lua/plenary.nvim'},
    config = function() req"cfg.telescope" end,
    module = 'telescope',
    cmd = { 'Telescope', 'TelescopeToggle' },
  } -- }}}
  --}}}]]
  --{{{ Not used
--[=[
  use { 'unblevable/quick-scope', -- {{{ --- horizonal movement
    setup = function()
      vim.g.qs_highlight_on_keys = {'f', 'F', 't', 'T'}
      vim.g.qs_second_highlight = 0
      vim.g.qs_hi_priority = 9
      -- vim.g.qs_enable
      -- vim.b.qs_local_disable
      -- vim.g.qs_max_chars
      -- vim.g.qs_buftype_blacklist = {'terminal', 'nofile'}
      -- g:qs_filetype_blacklist = ['dashboard', 'startify']
      -- g:qs_accepted_chars = [ 'a', 'b', ... etc ]
      -- g:qs_lazy_highlight = 1
      --[[
        highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
        highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
      --]]
    end
  } -- }}}
  use {'gcmt/wildfire.vim', event = "BufRead"} -- Select text object
  use {"tpope/vim-surround", after = "wildfire.vim"} -- surrounding select text with given text
-- use { 'wellle/targets.vim' } -- adds text objects to ease, for example, edit in pairs of quotes and parentheses
--]=]
  --}}}
  -- }}}

  -- [[ Terminals ]] {{{
  -- use { 'voldikss/vim-floaterm' }
  use { 'numtostr/FTerm.nvim', -- {{{
    config = function()
      req"cfg.fterm"
    end,
    cmd = {'FTermToggle', 'LazygitToggle'},
  } -- }}}
  -- use {'kassio/neoterm', cmd = {'T', "Tkill", "Tclose", "Tmap"}}
  -- }}}

  -- [[ Unsorted stuff ]] {{{
  use { 'nvim-lua/popup.nvim' }
  use {'lambdalisue/suda.vim', cmd = {'SudaWrite', 'SudaRead'}} -- reopen/force-write file when forget sudo
  use { 'famiu/nvim-reload', -- {{{
    cmd = {"Reload", "Restart"},
    requires = "nvim-lua/plenary.nvim"
  } -- }}}
  use { 'ruanyl/vim-gh-line' }
  use { 'MattesGroeger/vim-bookmarks' }
  use { 'vim-scripts/genutils' }
  -- IndentLine {{{
  use { 'lukas-reineke/indent-blankline.nvim',
    config = function()
      req"cfg.indent-blankline"
    end
  }
  -- }}}
  --[[ use { 'lewis6991/gitsigns.nvim', -- {{{
    requires = { 'nvim-lua/plenary.nvim' },
    event = 'BufRead',
    config = function() -- {{{
      req"cfg.gitsigns"
    end -- }}}
  } -- }}}]]
  use { 'mhinz/vim-signify', -- {{{
    setup = function()
      req"cfg.signify"
    end,
  }
  -- }}}

  -- Neorg {{{
  use { 'nvim-neorg/neorg', -- {{{
    branch = 'main',
    config = function() -- {{{ -- TODO: move to separate config
      require"neorg".setup { -- {{{
        load = { -- {{{
          ["core.defaults"] = {}, -- Load all the defaults
          ["core.norg.concealer"] = {}, -- Allows the use of icons
          ["core.keybinds"] = { config = { default_keybinds = true, neorg_leader = "<leader>o" } },
          ["core.gtd.base"] = { config = { workspace = "gtd" } },
          ["core.integrations.treesitter"] = { -- {{{
            config = { -- {{{
              highlights = { -- {{{
                Unordered = { -- {{{
                  List = { -- {{{
                    ["1"] = "+NeorgHeading1Title", ["2"] = "+NeorgHeading2Title",
                    ["3"] = "+NeorgHeading3Title", ["4"] = "+NeorgHeading4Title",
                    ["5"] = "+NeorgHeading5Title", ["6"] = "+NeorgHeading6Title",
                  }, -- }}}
                  Link = { -- {{{
                    ["1"] = "+htmlh1", ["2"] = "+htmlh2",
                    ["3"] = "+htmlh3", ["4"] = "+htmlh4",
                    ["5"] = "+htmlh5", ["6"] = "+htmlh6",
                  }, -- }}}
                }, -- }}}
                Ordered = { -- {{{
                  List = { -- {{{
                    ["1"] = "+NeorgHeading1Title", ["2"] = "+NeorgHeading2Title",
                    ["3"] = "+NeorgHeading3Title", ["4"] = "+NeorgHeading4Title",
                    ["5"] = "+NeorgHeading5Title", ["6"] = "+NeorgHeading6Title",
                  }, -- }}}
                  Link = { -- {{{
                    ["1"] = "+htmlh1", ["2"] = "+htmlh2",
                    ["3"] = "+htmlh3", ["4"] = "+htmlh4",
                    ["5"] = "+htmlh5", ["6"] = "+htmlh6",
                  }, -- }}}
                }, -- }}}
                Quote = { -- {{{
                  ["1"] = { [""] = "+htmlH1", Content = "+htmlH1" },
                  ["2"] = { [""] = "+htmlH2", Content = "+htmlH2" },
                  ["3"] = { [""] = "+htmlH3", Content = "+htmlH3" },
                  ["4"] = { [""] = "+htmlH4", Content = "+htmlH4" },
                  ["5"] = { [""] = "+htmlH5", Content = "+htmlH5" },
                  ["6"] = { [""] = "+htmlH6", Content = "+htmlH6" },
                }, -- }}}
                Definition = { --{{{
                  [""] = "+Exception",
                  End = "+Exception",
                  Title = "+TSStrong",
                  -- TODO: figure out odd highlighting of ranged tag when using TSNone
                  Content = "+TSEmphasis",
                }, -- }}}
                TodoItem = { -- {{{
                  ["1"] = { -- {{{
                    [""] = "+NeorgUnorderedList1",
                    Undone = "+StringDelimiter",
                    Pending = "+TSPunctDelimiter",
                    Done = "+TSString",
                  }, -- }}}
                  ["2"] = { -- {{{
                    [""] = "+NeorgUnorderedList2",
                    Undone = "+StringDelimiter",
                    Pending = "+TSPunctDelimiter",
                    Done = "+TSString",
                  }, -- }}}
                  ["3"] = { -- {{{
                    [""] = "+NeorgUnorderedList3",
                    Undone = "+StringDelimiter",
                    Pending = "+TSPunctDelimiter",
                    Done = "+TSString",
                  }, -- }}}
                  ["4"] = { -- {{{
                    [""] = "+NeorgUnorderedList4",
                    Undone = "+StringDelimiter",
                    Pending = "+TSPunctDelimiter",
                    Done = "+TSString",
                  }, -- }}}
                  ["5"] = { -- {{{
                    [""] = "+NeorgUnorderedList5",
                    Undone = "+StringDelimiter",
                    Pending = "+TSPunctDelimiter",
                    Done = "+TSString",
                  }, -- }}}
                  ["6"] = { -- {{{
                    [""] = "+NeorgUnorderedList6",
                    Undone = "+StringDelimiter",
                    Pending = "+TSPunctDelimiter",
                    Done = "+TSString",
                  }, -- }}}
                }, -- }}}
                EscapeSequence = "+TSType",
                StrongParagraphDelimiter = "+Comment",
                WeakParagraphDelimiter = "+Comment",
                HorizontalLine = "+htmlH4",
                Marker = { [""] = "+Structure", Title = "+TSStrong" },
                Tag = { -- {{{
                  Begin = "+TSKeyword",
                  ["End"] = "+TSKeyword",
                  Name = { [""] = "+Normal", Word = "+TSKeyword" },
                  Parameter = "+TSType",
                  Content = "+Normal",
                }, -- }}}
                Insertion = { -- {{{
                  [""] = "cterm=bold gui=bold",
                  Prefix = "+TSPunctDelimiter",
                  Variable = {
                    [""] = "+TSString",
                    Value = "+TSPunctDelimiter",
                  },
                  Item = "+TSNamespace",
                  Parameters = "+TSComment",
                }, -- }}}
              } -- }}}
            } -- }}}
          }, -- }}}
          ["core.norg.dirman"] = { -- Manage Neorg directories -- {{{
            config = { -- {{{
              workspaces = { -- {{{ -- TODO: configure real paths
                main   = "/tmp/dev/neorg",
                work   = "/tmp/dev/neorg/work",
                school = "/tmp/dev/neorg/school",
              }, -- }}}
              autochdir = false,
              autodetect = false
            } -- }}}
          }, -- }}}
          -- ["core.integrations.telescope"] = {},
          ["core.norg.completion"] = { config = { engine = "nvim-cmp", } },
        }, -- }}}
        logger = { level = "warn" },
      } -- }}}
    end, -- }}}
    after = "nvim-treesitter"
  } -- }}}
  -- }}}
  use { 'TimUntersberger/neogit', -- {{{
    requires = {'nvim-lua/plenary.nvim', 'sindrets/diffview.nvim'},
    config = function()
      req'neogit'.setup{
        integrations = {diffview = true},
        -- Change the default way of opening neogit
        kind = "split_above",
        -- customize displayed signs
        signs = {
          -- { CLOSED, OPENED },
          section = {"", ""},
          item = {"", ""},
          hunk = {"", ""},
        },
      }
    end,
    cmd = "Neogit"
  } -- }}}
  use { 'mhinz/vim-sayonara', -- {{{
    --[[ setup = function()
   --   vim.g.sayonara_confirm_quit = 1
    end, ]]
    -- cmd = 'Sayonara'
  } -- }}}

--[=[ -- {{{ Unused stuff
--[[
  use { 'nathom/filetype.nvim', -- {{{ TODO: port gentoo ft stuff to this
    config = function()
      req"filetype".setup({
        -- overrides the filetype or function for filetype
        -- See https://github.com/nathom/filetype.nvim#customization
        overrides = {},
      })
    end
  } -- }}}
 ]]
  use { 'junegunn/goyo.vim' }                 --- hide all the things except code
  use { 'xiyaowong/nvim-transparent' }        --- transparency
  use { 'airblade/vim-rooter', -- {{{
    cmd = "Rooter",
    setup = function()
        vim.g.rooter_manual_only = 1
        vim.g.rooter_change_directory_for_non_project_files = 'current'
        vim.g.rooter_patterns = {'.git', 'Cargo.toml'}
    end
  } -- }}}
  use { 'tpope/vim-fugitive', -- {{{
    cmd = {'G', 'Git', 'Ggrep', 'Gdiffsplit', 'GBrowse'},
  } -- }}}
  --[[ -- {{{
  -- Dependency: tmux, nnn
  { "luukvbaal/nnn.nvim", -- {{{
    config = function()
      req"nnn".setup({
        picker = {
          cmd = [[NNN_PLUG="p:preview-tui" ICONLOOKUP=1 tmux new-session nnn -a -Pp]],
          style = {border = "shadow"},
          session = "shared"
        },
      })
    end,
    cmd = {'NnnPicker', 'NnnExplorer'},
  }, -- }}}
  --]] -- }}}
  -- use { 'Shougo/neco-syntax' }
  -- use { 'Shougo/neoinclude.vim' }
  -- use { 'Shougo/neco-vim' }
  -- use { 'Shougo/neopairs.vim' }
  -- use { 'powerman/vim-plugin-ruscmd' } -- SSSSSSSSSSSSSOOOOOOOOOO SSSSSSSSSSLLLLLLLLLOOOOOOOOOOOWWWWWWWWWWW

  -- use { 'mhinz/vim-startify' }
  --[[{{{
  use { 'glepnir/dashboard-nvim', -- {{{
    cmd = {"Dashboard"},
    config = function() req"cfg.dashboard" end
  } -- }}}
  --}}}]]


  use { "max397574/better-escape.nvim", -- {{{
    config = function()
      req"better_escape".setup {
          mapping = {"jj"}, -- a table with mappings to use
          timeout = vim.o.timeoutlen, -- the time in which the keys must be hit in ms. Use option timeoutlen by default
          clear_empty_lines = true, -- clear line after escaping if there is only whitespace
          keys = "<Esc>", -- keys used for escaping, if it is a function will use the result everytime
      }
    end
  } -- }}}
--]=] -- }}}
-- }}}
  end, -- }}}
  config = {
    -- Move to data dir to work fine with symlinked setup when multiple users use it
    compile_path = packer_compiled_path,
    profile = {
      enable = true,
      threshold = 1 -- the amount in ms that a plugins load time must be over for it to be included in the profile
    },
    -- vim.fn.stdpath('data')..'/packer_compiled.lua'
  }
}) -- }}}
