-- luacheck: globals req vim
local M = {}
local packer_repo = "https://github.com/wbthomason/packer.nvim"

local f = req"lib.funcs"

function M:before() -- luacheck: no unused args

end
function M:after() -- luacheck: no unused args
--    vim.cmd[[autocmd VimEnter * Rooter]]
--    req"cfg.colors"
end

function M:load() -- {{{ -- luacheck: no unused args
  local firstrun = false
--  local install_path = ("%s/site/pack/packer/start/packer.nvim"):format(vim.fn.stdpath("data"))
  local install_path = ("%s/site/pack/packer/opt/packer.nvim"):format(vim.fn.stdpath("data"))

  if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
      f.einfo(("Installing packer to %s"):format(install_path))
      firstrun = vim.fn.system({
          'git', 'clone', '--depth', '1',
          packer_repo, install_path
      })
  end

  local ok, err = pcall(vim.cmd, [[packadd packer.nvim]])
  if not ok then
      f.eerror(("Caught following error while loading packer: %s"):format(err), "plugins")
      return
  end

  self:before()

  local plugins = req'cfg.plugins' -- luacheck: no unused

  vim.cmd([[autocmd BufWritePost *lua/cfg/plugins.lua source <afile> | PackerSync]]) -- move to aus config?

  if firstrun then
    req('packer').sync()
    --plugins.sync()
    return
  end
  self:after()
end -- }}}

return M

