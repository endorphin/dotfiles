ZSH_HIGHLIGHT_HIGHLIGHTERS+=( brackets pattern )
# cursor )

ZSH_HIGHLIGHT_PATTERNS+=('rm -rf *' 'fg=white,bg=red')
ZSH_HIGHLIGHT_PATTERNS+=('rm -Rf *' 'fg=white,bg=red')
ZSH_HIGHLIGHT_PATTERNS+=('rm -fr *' 'fg=white,bg=red')
ZSH_HIGHLIGHT_PATTERNS+=('rm -fR *' 'fg=white,bg=red')

#ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=red'
#ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=cyan'
#ZSH_HIGHLIGHT_STYLES[alias]='fg=green'
#ZSH_HIGHLIGHT_STYLES[builtin]='fg=green'
#ZSH_HIGHLIGHT_STYLES[function]='fg=green'
#ZSH_HIGHLIGHT_STYLES[command]='fg=green'
#ZSH_HIGHLIGHT_STYLES[precommand]='fg=green'
#ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=green'
#ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=green'
ZSH_HIGHLIGHT_STYLES[path]='fg=yellow'
#ZSH_HIGHLIGHT_STYLES[globbing]='fg=green'
#ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=green'
#ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=darkgray'
#ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=darkgray'
#ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=green'
#ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=green'
#ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=green'
#ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=green'
#ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=green'
#ZSH_HIGHLIGHT_STYLES[assign]='fg=green'
ZSH_HIGHLIGHT_STYLES[default]='fg=white'
